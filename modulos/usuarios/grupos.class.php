<?php
header("Content-Type: text/html;charset=utf-8");

class GRUPOS{

	var $fmt;
	var $id_mod;
	var $id_item;
	var $id_estado;
	var $ruta_modulo;

	function GRUPOS($fmt,$id_mod=0,$id_item=0,$id_estado=0){
		$this->fmt = $fmt;
		$this->id_mod = $id_mod;
		$this->id_item = $id_item;
		$this->id_estado = $id_estado;
		$this->ruta_modulo= _RUTA_WEB."dashboard/".$this->fmt->class_modulo->ruta_amigable_modulo($id_mod);
	}

	function busqueda(){
		$this->fmt->class_pagina->crear_head( $this->id_mod,$botones);
		$this->fmt->class_pagina->head_mod();
    $this->fmt->class_pagina->head_modulo_inner("Lista de Grupos", $botones,"crear",$this->id_mod); // bd, id modulo, botones

    $this->fmt->form->head_table("table_id");
    $this->fmt->form->thead_table('Id:Nombre:Detalle:Activar:Acciones');
    $this->fmt->form->tbody_table_open();

  	$consulta = "SELECT * FROM grupo";

  	$rs =$this->fmt->query->consulta($consulta);
  	$num=$this->fmt->query->num_registros($rs);
  	if($num>0){
  		for($i=0;$i<$num;$i++){
  			$row=$this->fmt->query->obt_fila($rs);
  			$row_id = $row["grupo_id"];
  			$row_nombre = $row["grupo_nombre"];
  			$row_detalle= $row["grupo_detalle"];
  			$row_activar = $row["grupo_activar"];

				echo "<tr class='row row-".$row_id."'>";
				echo '  <td class="col-id">'.$row_id.'</td>';
				echo '  <td class="col-name">'.$row_nombre.'</span></td>';
				echo '  <td class="col">'.$row_detalle.'</span></td>';
				echo '  <td class="col-activar">';
				 $this->fmt->class_modulo->estado_publicacion($row_activar,$this->id_mod,"",$row_id);
				echo '	</td>';
				echo '  <td class="col-acciones acciones">';
				$this->fmt->class_modulo->botones_tabla($row_id,$this->id_mod,$row_nombre);//
				echo '	</td>';
  		}
  	}
  	$this->fmt->query->liberar_consulta();
		
		$this->fmt->form->tbody_table_close();
    $this->fmt->form->footer_table();
		
		$this->fmt->class_pagina->footer_mod();
    $this->fmt->class_modulo->script_table("table_id",$this->id_mod,"desc","0","10",true);
    $this->fmt->class_pagina->footer_mod();
		$this->fmt->class_modulo->script_accion_modulo();
	}

	function form_nuevo(){
		$this->fmt->class_pagina->crear_head_form("Nuevo Grupo","","");
		$id_form="form-nuevo";

		$this->fmt->class_pagina->head_form_mod();
		$this->fmt->class_pagina->form_ini_mod($id_form,"form-cursos");
		$this->fmt->form->input_form("<span class='obligatorio'>*</span> Nombre:","inputNombre","","","input-lg","","");
		$this->fmt->form->textarea_form('Detalle:','inputDetalle','','','','','3','','');
		$this->fmt->form->textarea_form('Funciones:','inputFunciones','','','','','3','','');
		$this->fmt->form->botones_nuevo($id_form,$this->id_mod,"","ingresar");
		$this->fmt->class_modulo->modal_script($this->id_mod);
	}


	function form_editar(){
    $this->fmt->class_pagina->crear_head_form("Editar Grupo","","");
		$id = $this->id_item;
    $consulta ="SELECT * FROM grupo WHERE grupo_id='$id'";
    $rs = $this->fmt->query->consulta($consulta);
    $fila=  $this->fmt->query->obt_fila($rs);

    $this->fmt->class_pagina->head_form_mod();
		$this->fmt->class_pagina->form_ini_mod($id_form,"form-certificado");

    $this->fmt->form->input_form('Nombre','inputNombre','', $fila['grupo_nombre'],'input-lg','',''); //$label,$id,$placeholder,$valor,$class,$class_div,$mensaje
    $this->fmt->form->input_hidden_form('inputId',$fila['grupo_id']);
    $this->fmt->form->textarea_form('Detalles:','inputDetalles','', $fila['grupo_detalles'],'','','3',''); //$label,$id,$placeholder,$valor,$class,$rows,$mensajes
    $this->fmt->form->textarea_form('Funciones','inputFunciones','', $fila['grupo_funciones'],'','','3',''); //$label,$id,$placeholder,$valor,$class,$rows,$mensajes

    $this->fmt->form->btn_actualizar($id_form,$this->id_mod,"modificar");

    $this->fmt->class_pagina->form_fin_mod();
		$this->fmt->class_pagina->footer_form_mod();
 
    $this->fmt->class_modulo->modal_script($this->id_mod);
  }


	function ingresar(){

		if ($_POST["btn-accion"]=="activar"){
			$activar=1;
		}
		if ($_POST["btn-accion"]=="guardar"){
			$activar=0;
		}

		$ingresar ="grupo_nombre,grupo_detalle,grupo_funciones,grupo_activar";
		$valores  ="'".$_POST['inputNombre']."','".
						$_POST['inputDetalle']."','".
						$_POST['inputFunciones']."','".
						$activar."'";

		$sql="insert into grupo (".$ingresar.") values (".$valores.")";

		$this->fmt->query->consulta($sql);

		$this->fmt->class_modulo->redireccionar($ruta_modulo,"1");	

	} // fin funcion ingresar



}
