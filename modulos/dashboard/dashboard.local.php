<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 1 Jul 2500 05:00:00 GMT"); // Fecha en el pasado

require_once (_RUTA_NUCLEO . "controler/class/class-constructor.php");
$fmt = new CONSTRUCTOR();
/* ----- Saltar al login si no se esta en sesión ----- */
if ($fmt->sesion->get_variable("usu_id") == false) {
  $ruta_rol = _RUTA_LOGOUT;
  ?>
<script type="text/javascript" language="javascript" src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/core.js"></script>
<script type="text/javascript">
redireccionar_tiempo("<?php echo $ruta_rol; ?>", 1);
</script>
<?php
}
$userId = $fmt->sesion->get_variable("usu_id");
$rolId = $fmt->usuario->id_rol_usuario($userId);



echo $fmt->header->header_html('0');
// echo $fmt->header->js_jquery();
$fmt->header->titlePage("Dashboard");
?>
<script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/jquery.min.js"></script>
<link href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/summernote-bs3.css" rel="stylesheet" />
<link href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/summernote.css" rel="stylesheet">
<script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/summernote.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
<script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/xlsx.full.min.js"></script>
<script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/FileSaver.min.js"></script>
<script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/tableexport.min.js"></script>


<link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/icon-font.css?reload" rel="stylesheet"
    type="text/css">
<link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/estilos.adm.css?reload" rel="stylesheet"
    type="text/css">

<link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/theme.adm.css?reload" rel="stylesheet"
    type="text/css">

<link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/dist/nav.adm.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/nav-theme.adm.css" rel="stylesheet" type="text/css">

</head>

<body class='body-dashboard'>

    <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/animate.css?reload" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/bootstrap-datetimepicker.min.css?reload"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/datetimepicker.adm.css?reload" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/bootstrap-glyphicons.css" rel="stylesheet"
        type="text/css">



    <?php if (_THEME_DEFAULT_ADMIN) { ?>
    <link rel="stylesheet" href="<?php echo _RUTA_WEB . _THEME_DEFAULT_ADMIN; ?>?reload" rel="stylesheet"
        type="text/css">
    <?php } ?>

    <div class='preloader-modulo'></div>
    <?php

  require_once (_RUTA_NUCLEO . 'modulos/nav/navbar.adm.php');

  $das = 0;



  if ($_GET["m"]) {
    $sql = "SELECT mod_url,mod_id,mod_ruta_amigable,mod_tipo FROM modulo WHERE mod_ruta_amigable='" . $_GET["m"] . "'";
    $rs = $fmt->query->consulta($sql, __METHOD__);
    $row = $fmt->query->obt_fila($rs);
    $urlMod = $row["mod_url"];
    $modId = $row["mod_id"];

    //echo "modId:" . $modId . '</br>';
    //echo $userId . " " . $rolId;


    if ($rolId != 1) {
      $sql = "SELECT * FROM modulo_roles WHERE mod_rol_rol_id='" . $rolId . "' AND mod_rol_mod_id='" . $modId . "'";
      $rs = $fmt->query->consulta($sql, __METHOD__);
      $num = $fmt->query->num_registros($rs);
      if ($num == 0) {
        echo "<div class='' style='text-align:center; margin-top:100px'>
                  <script> $('.preloader-modulo').remove();</script>
                  <h3>Error de permisos. " . $num . "</h3>
                </div>";
        exit();
      }
    }

    //echo $urlMod;
  
    // if ($row["mod_tipo"]!=4){
    //   $url_mod = _RUTA_NUCLEO."".$row["mod_url"];
    //   $url = _RUTA_NUCLEO."".$row["mod_url"];
    // }else{
    //   $url_mod = _RUTA_HOST."".$row["mod_url"];
    //   $url = _RUTA_HOST."".$row["mod_url"];
    // }
  
    echo '<div class="container-fluid content-page" id="' . $row["mod_ruta_amigable"] . '">';


    if (strpos($urlMod, '{_') !== false) {
      $url = str_replace("{_RUTA_WEB}", _RUTA_WEB, $urlMod);
      $url = str_replace("{_RUTA_NUCLEO}", _RUTA_NUCLEO, $url);
      $url = str_replace("{_RUTA_HOST}", _RUTA_HOST, $url);
    } else {
      $url = _RUTA_NUCLEO . "" . $urlMod;
    }

    $url_a = $row["mod_ruta_amigable"];

    $sqle = "UPDATE configuracion SET
                  conf_last_modulo='" . $row["mod_id"] . "'";

    $fmt->query->consulta($sqle);

    //echo $url;
    //echo _RUTA_HOST;
  
    //echo file_exists($url);
  
    if (file_exists($url)) {
      $id_mod = $row["mod_id"];
      $idMod = $row["mod_id"];


      require_once ($url);
    } else {
      $fmt->errores->error_pag_no_encontrada();
    }
    $das = 1;
  }

  if ($_GET["app"]) {
    $sql = "SELECT app_url,app_nombre,app_id,app_ruta_amigable FROM aplicacion WHERE app_ruta_amigable='" . $_GET["app"] . "'";
    $rs = $fmt->query->consulta($sql, __METHOD__);
    $row = $fmt->query->obt_fila($rs);
    $app_url = $row["app_url"];

    $url_app = str_replace("{_RUTA_WEB}", _RUTA_WEB, $app_url);
    $url_app = str_replace("{_RUTA_NUCLEO}", _RUTA_NUCLEO, $url_app);
    $url_app = str_replace("{_RUTA_HOST}", _RUTA_HOST, $url_app);

    echo '<div class="container-fluid content-page" id="' . $row["app_ruta_amigable"] . '">';

    $url_a = $row["app_ruta_amigable"];
    if (file_exists($url_app)) {
      $id_app = $row["app_id"];
      $idApp = $row["app_id"];
      $fmt->header->title_page($row["app_nombre"]);
      require_once ($url_app);
    } else {
      $fmt->errores->error_pag_no_encontrada();
    }
  }
  ?>
    <!-- <a class="btn-menu-ajax" id_mod="27" vars="" >hello</a> -->
    <?php

  if (($das == 0) && (_RUTA_DASHBOARD != "")) {
    require_once (_RUTA_DASHBOARD);
  }

  ?>

    </div><!--  content-pag -->
    <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/modal.adm.css?reload" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/modal-theme.adm.css?reload" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/finder.css?reload" rel="stylesheet"
        type="text/css">

    <script>
    var sitio = "<?php echo _RUTA_WEB; ?>";
    var sitioNucleo = "<?php echo _RUTA_WEB_NUCLEO; ?>";
    </script>


    <div class="modal modal-eliminar">
        <div class="modal-eliminar-inner"></div>
    </div>
    <div class="modal modal-list">
        <div class="modal-list-inner"></div>
    </div>
    <div class="modal modal-editar">
        <div class="modal-editar-inner"></div>
    </div>
    <div class="modal modal-finder">
        <div class="modal-finder-inner"></div>
    </div>
    <div class="modal modal-form modal-m-<?php echo $url_a; ?>" id="modal">
        <div class="modal-inner">
            <div class="preloader-page"></div>
        </div>
    </div>

    <script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/jquery-ui.min.js"></script>
    <script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/bootstrap.js"></script>
    <script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/summernote.js"></script>



    <script type="text/javascript" src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/bootstrap-datetimepicker.es.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/core.js?reload">
    </script>

</body>

</html>