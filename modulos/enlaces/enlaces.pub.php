<?php
	header("Content-Type: text/html;charset=utf-8");
	require_once(_RUTA_NUCLEO."clases/class-constructor.php");
	$fmt = new CONSTRUCTOR;
	$fmt->publicacion->publicacion($fmt,$pub_id);
	$pub_nom=$fmt->publicacion->get_pub_nombre();
	$pub_titulo=$fmt->publicacion->get_pub_titulo();
	$pub_id_cat=$fmt->publicacion->get_pub_id_cat();
	$pub_num=$fmt->publicacion->get_pub_numero_items();
?>
<div class="pub pub-<?php echo $pub_nom; ?>">
	<div class="pub-title"><?php echo $pub_titulo; ?></div>
	<div class="pub-inner">
		<?php 

			$html ='';

			if (!empty($limite)) {
	      $lim = 'LIMIT 0,'.$pub_num;
	    }else{
	      $lim = '';
	    }
	    
	    $consulta = "SELECT * FROM  enlace, enlace_publicaciones WHERE enl_pub_pub_id='$pub_id' AND enl_pub_enl_id=enl_id AND enl_activar='1' ORDER BY enl_pub_orden ASC $lim";
	    $rs = $this->fmt->query->consulta($consulta);
	    $num= $this->fmt->query->num_registros($rs);
	    if($num>0){
	      for ($i=0; $i < $num; $i++) { 
	        $row = $this->fmt->query->obt_fila($rs);
	        $enl_id = $row["enl_id"];
	        $enl_nombre = $row["enl_nombre"];
	        $enl_descripcion = $row["enl_descripcion"];
	        $enl_link = $row["enl_link"];
	        $enl_target = $row["enl_target"];
	        $enl_imagen = _RUTA_IMAGES.$row["enl_imagen"];
	        $enl_clases = $row["enl_clases"];

	        $html .= '<div class="item item-'.$i.' '.$enl_clases.'" item="link-'.$enl_id.'">';
	        $html .= '	<a href="'.$enl_link.'" target="'.$enl_target.'">';
	        $html .= '		<div class="img"><img src="'.$enl_imagen.'" alt=""></div>';
	        $html .= '		<div class="title">'.$enl_nombre.'</div>';
	        $html .= '		<div class="descripcion">'.$enl_descripcion.'</div>';
	        $html .= '	</a>';
	        $html .= '</div>';
	      }
	      echo $html;
	    } 
	    $this->fmt->query->liberar_consulta();
		?>
	</div>
</div>