<?php
header("Content-Type: text/html;charset=utf-8");

class ATAJO{

    var $fmt;
    var $id_mod;
    var $id_item;
    var $id_estado;
    var $ruta_modulo;

    function ATAJO($fmt,$id_mod=0,$id_item=0,$id_estado=0){
        $this->fmt = $fmt;
        $this->id_mod = $id_mod;
        $this->id_item = $id_item;
        $this->id_estado = $id_estado;
        $this->ruta_modulo= _RUTA_WEB."dashboard/".$this->fmt->class_modulo->ruta_amigable_modulo($id_mod);
    }

    public function busqueda(){

        echo $this->fmt->page->headerPageMod(  array('id_mod' => $this->id_mod,
                                                'modo' => "estandar-mod",
                                                'css' => _RUTA_WEB_NUCLEO."src/css/atajos.css" )); 

        echo $this->fmt->page->headModuloInner( array(  'title' => 'Lista de Atajos',
                                                        'idMod' => $this->id_mod,
                                                        'btnDefault' => 'crear'));

        $this->fmt->form->head_table("table_id");
        $this->fmt->form->thead_table('Id:Url:Activar:Acciones');
        $this->fmt->form->tbody_table_open();


        $consulta = "SELECT * FROM atajo WHERE atj_usu_id='"._USU_ID."'";
        $rs =$this->fmt->query->consulta($consulta);
        $num=$this->fmt->query->num_registros($rs);
        $tabla="";
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->query->obt_fila($rs);
                
                $row_id = $row["atj_id"];
                $row_nombre = $row["atj_nombre"];
                $row_icono = $row["atj_icono"];
                $row_color = $row["atj_color"];
                $row_url = $row["atj_url"];
                $row_usu_id = $row["atj_usu_id"];
                $row_activar = $row["atj_activar"];

                $estado = $this->fmt->modulo->estadoPublicacion( array( 'estado' => $row_activar, 
                                                                        'id_mod'=> $this->id_mod,
                                                                        'class'=> '',
                                                                        'id'=> $row_id ));
                // $arrayModulo = $this->fmt->modulo->datosId($row_mod_id);
                $usu = $this->fmt->usuario->nombreCompleto($row_usu_id);

                // if ($row_url!=""){
                //     $url = '<i class="'.$row_icono.'"></i><span>'.$row_url.'</span>';
                // }else{
                //     $icono = $arrayModulo["mod_icono"];
                //     $link = _RUTA_WEB.$arrayModulo["mod_ruta_amigable"];
                //     $url = '<i class="'.$icono.'"></i><span>'.$link.'</span>';
                // }


                $tabla .= '<tr class="row row-'.$row_id.'">';
                $tabla .= '  <td class="col-id">'.$row_id.'</td>';
                $tabla .= '  <td class="col-nombre"><span style="color : '.$row_color.'">'.$row_nombre.' </span> : '.$row_icono.' : '.$row_url.'</td>';
                $tabla .= '  <td class="col-activar">'.$estado.'</td>';
                $tabla .= '  <td class="col-acciones">';
                $tabla .= $this->fmt->form->btnAccionTabla(array( 'idMod'=>$this->id_mod, 
                                                                'idItem' => $row_id,
                                                                'nombreItem' => $row_id ));
                $table .= '</td>';
                $tabla .= '</td>';
            }
        }

        echo $tabla;

        $this->fmt->form->tbody_table_close();
        $this->fmt->form->footer_table();
        echo $this->fmt->modulo->scriptPage(array("id_mod" => $this->id_mod));
        $this->fmt->class_modulo->script_table("table_id",$this->id_mod,"desc","2","25",true);
        //$this->fmt->class_modulo->script_accion_modulo();
        $this->fmt->page->footerPageMod();
    }

    public function form_nuevo(){

        $html ='';
        $idForm="form-nuevo";
        $html .= $this->fmt->page->modHead( array( 'title' => 'Nuevo Atajo','modo' => 'mod' ));

        $html .= $this->fmt->form->formIni($idForm,"formAtajo");
        $html .= $this->fmt->form->titleSeparador(array('title' => 'Crea un Atajo por URL','%'=> '40' ));
        $html .= $this->fmt->form->input( array('label' => 'Nombre:','id'=> 'inputNombre' ));
        $html .= $this->fmt->form->input( array('label' => 'Url Atajo:','id'=> 'inputUrl' ));
        $html .= $this->fmt->form->input( array('label' => 'Color:','id'=> 'inputColor' ));
        $html .= $this->fmt->form->inputIcono( array('label' => 'Icono:','id'=> 'inputIcono' ));
        $html .= $this->fmt->form->titleSeparador(array('title' => 'o','%'=> '10' ));
        $html .= $this->fmt->form->selectQuery( array('label' => 'seleccionar modulo:',
                                                        'query' => 'SELECT mod_id, mod_nombre FROM modulo WHERE mod_activar="1" ORDER BY mod_nombre',
                                                        'prefijo' => 'mod_',
                                                        'labelValor0' => 'Seleccionar un modulo', 
                                                        'id'=> 'inputModulo' ));
        $html .= $this->fmt->form->separador();
        $html .= $this->fmt->form->select( array('label' => 'Menú:',
                                                    'valores'=>'0,1',
                                                    'options'=>'Menú Top,Menú Lateral',
                                                    'classSelect' => 'box-md-3',
                                                    'id'=>'inputTipo'));

        $html .= $this->fmt->form->btnAccionForm( array( 'idMod' => $this->id_mod,
                                                    'form' => $idForm,
                                                    'tarea'=>'ingresar' ));
        $html .= $this->fmt->form->formEnd();
        
        $html .= $this->fmt->page->modFooter(array('modo' => 'mod'));
        $html .= $this->fmt->modulo->modalEditorTexto("inputDetalles","300");
        $html .= $this->fmt->modulo->scriptPage(array("id_mod" => $this->id_mod));

        echo $html;
    }

    public function form_editar(){

        $html ='';
        $idForm="form-editar";
        $id = $this->id_item;

        $consulta = "SELECT * FROM atajo WHERE atj_id='".$id."'";
        $rs =$this->fmt->query->consulta($consulta);
        $row=$this->fmt->query->obt_fila($rs);

        $html .= $this->fmt->page->modHead( array( 'title' => 'Editar Atajo','modo' => 'mod' ));

        $html .= $this->fmt->form->formIni($idForm,"formAtajo");
        $html .= $this->fmt->form->titleSeparador(array('title' => 'Crea un Atajo por URL','%'=> '40' ));
        $html .= $this->fmt->form->inputHidden('inputId',$id);
        $html .= $this->fmt->form->input( array('label' => 'Nombre:','id'=> 'inputNombre','value'=> $row["atj_nombre"] ));
        $html .= $this->fmt->form->input( array('label' => 'Url Atajo:','id'=> 'inputUrl','value'=> $row["atj_url"]));
        $html .= $this->fmt->form->input( array('label' => 'Color:','id'=> 'inputColor','value'=> $row["atj_color"]));
        $html .= $this->fmt->form->inputIcono( array('label' => 'Icono:','id'=> 'inputIcono' ,'value'=> $row["atj_icono"]));
        $html .= $this->fmt->form->titleSeparador(array('title' => 'o','%'=> '10' ));
        $html .= $this->fmt->form->selectQuery( array('label' => 'seleccionar modulo:',
                                                        'query' => 'SELECT mod_id, mod_nombre FROM modulo WHERE mod_activar="1" ORDER BY mod_nombre',
                                                        'prefijo' => 'mod_',
                                                        'labelValor0' => 'Seleccionar un modulo', 
                                                        'id'=> 'inputModulo' ));
        $html .= $this->fmt->form->separador();
        $html .= $this->fmt->form->select( array('label' => 'Menú:',
                                                    'valores'=>'0,1',
                                                    'options'=>'Menú Top,Menú Lateral',
                                                    'seleccionado'=>$row["atj_tipo"],
                                                    'classSelect' => 'box-md-3',
                                                    'id'=>'inputTipo'));

        $html .= $this->fmt->form->btnAccionForm( array( 'idMod' => $this->id_mod,
                                                    'form' => $idForm,
                                                    'botones' => 'actualizar',
                                                    'tarea'=>'modificar' ));
        $html .= $this->fmt->form->formEnd();
        
        $html .= $this->fmt->page->modFooter(array('modo' => 'mod'));
        $html .= $this->fmt->modulo->modalEditorTexto("inputDetalles","300");
        $html .= $this->fmt->modulo->scriptPage(array("id_mod" => $this->id_mod));

        echo $html;
    }

    public function ingresar(){

        if ($_POST["estado-mod"]=="activar"){
            $activar=1;
        }else{
            $activar=0;
        }
        $idUsu = $this->fmt->sesion->get_variable('usu_id');

        $ingresar ="atj_nombre,atj_tipo,atj_url,atj_usu_id,atj_icono,atj_color,atj_activar";

        $mod_id = $_POST["inputModulo"];

        if ($mod_id!="0"){
                $arrayModulo = $this->fmt->modulo->datosId($mod_id);
                $nombre = $arrayModulo["mod_nombre"];
                $icono = $arrayModulo["mod_icono"];
                $color = $arrayModulo["mod_color"];
                $url = _RUTA_WEB."dashboard/".$arrayModulo["mod_ruta_amigable"];
        }else{
            $nombre = $_POST["inputNombre"];        
            $icono = $_POST["inputIcono"];        
            $url = $_POST["inputUrl"];
            $color = $_POST["inputColor"];
        }


        $valores  ="'".$nombre."','".
                    $_POST['inputTipo']."','".
                    $url."','".
                    $idUsu."','".
                    $icono."','".
                    $color."','".
                    $activar."'";

        $sql="insert into atajo (".$ingresar.") values (".$valores.")";
         
        $this->fmt->query->consulta($sql);
        echo $this->fmt->modulo->redireccionar($ruta_modulo,"1");  
    }

    public function modificar(){

        $mod_id = $_POST["inputModulo"];

        if ($mod_id!="0"){
            $arrayModulo = $this->fmt->modulo->datosId($mod_id);
            $nombre = $arrayModulo["mod_nombre"];
            $icono = $arrayModulo["mod_icono"];
            $color = $arrayModulo["mod_color"];
            $url = _RUTA_WEB."dashboard/".$arrayModulo["mod_ruta_amigable"];
        }else{
            $nombre = $_POST["inputNombre"];        
            $icono = $_POST["inputIcono"];        
            $url = $_POST["inputUrl"];
            $color = $_POST["inputColor"];
        }

        $sql="UPDATE atajo SET
            atj_nombre='".$nombre."',
            atj_icono ='".$icono."',
            atj_url ='".$url."',
            atj_color ='".$color."',
            atj_tipo='".$tipo."'
            WHERE atj_id='".$_POST['inputId']."'";
        $this->fmt->query->consulta($sql,__METHOD__);
        echo $this->fmt->modulo->redireccionar($ruta_modulo,"1");  
    }

    public function datosMenu($tipo="0",$idUsu){
        if ($idUsu=="") {
            return 0;
        }else{
            $consulta = "SELECT * FROM atajo WHERE atj_usu_id='".$idUsu."' AND atj_tipo='".$tipo."'";
        }
        
        $rs =$this->fmt->query->consulta($consulta);
        $num=$this->fmt->query->num_registros($rs);
        $tabla="";
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->query->obt_fila($rs);
                
                $vars[$i]["atj_id"] = $row["atj_id"];
                $vars[$i]["atj_nombre"] = $row["atj_nombre"];
                $vars[$i]["atj_icono"] = $row["atj_icono"];
                $vars[$i]["atj_color"] = $row["atj_color"];
                $vars[$i]["atj_url"] = $row["atj_url"];
                $vars[$i]["atj_usu_id"] = $row["atj_usu_id"];
                $vars[$i]["atj_activar"] = $row["atj_activar"];

            }

            return $vars;
        }else{
            return 0;
        }
       $this->fmt->query->liberar_consulta($rs);
    }
}