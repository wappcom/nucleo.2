<?php
require_once(_RUTA_NUCLEO . "controler/class/class-constructor.php");
$fmt = new CONSTRUCTOR();

if ($fmt->sesion->get_variable("usu_id") != false) {
  $id_usu = $fmt->sesion->get_variable("usu_id");
  $id_rol = $fmt->usuario->id_rol_usuario($id_usu);
  $ruta_rol = _RUTA_WEB . $fmt->usuario->traer_ruta_rol($id_rol);
  if ($ruta_rol == "") {
    echo $fmt->errores->error_rol();
    exit(0);
  } else {
?>
    <script type="text/javascript" language="javascript" src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/core.js"></script>
    <script type="text/javascript">
      console.log("redireccionamiento");
      redireccionar_tiempo("<?php echo $ruta_rol; ?>", 1);
    </script>
<?php
  }
}

echo $fmt->header->header_html();
echo $metaLogin;


//$fmt->header->title_page("Logeo - Next Sistemas Integrados");
?>
<script src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/jquery.min.js"></script>
</head>

<body class='body-login container-fluid'>

  <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/estilos.adm.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/theme.adm.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/icon-font.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/animate.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/login.adm.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<?php echo _RUTA_WEB_NUCLEO; ?>src/css/login-theme.adm.css" rel="stylesheet" type="text/css">
  <?PHP if (_THEME_DEFAULT) { ?>
    <link rel="stylesheet" href="<?php echo _THEME_DEFAULT; ?>" rel="stylesheet" type="text/css">
  <?php } ?>
  <?php
  //echo $fmt->header->js_jquery();
  require_once(_RUTA_NUCLEO . "modulos/login/login.form.php");
  ?>

  <div class="login-footer">
    <?php echo _PIE_PAGINA; ?>
  </div>
  <script type="text/javascript" language="javascript" src="<?php echo _RUTA_WEB_NUCLEO; ?>src/js/core.js"></script>
</body>

</html>