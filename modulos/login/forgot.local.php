<?php
require_once(_RUTA_NUCLEO."controler/class/class-constructor.php");
$fmt = new CONSTRUCTOR();

echo $fmt->header->header_html();

?>
<link rel="shortcut icon" href="<?php echo _RUTA_WEB; ?>src/img/favicon.png" />
</head>
<body class='body-login container-fluid'>

  <link rel="stylesheet" href="<? echo _RUTA_WEB_NUCLEO; ?>src/css/estilos.adm.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<? echo _RUTA_WEB_NUCLEO; ?>src/css/theme.adm.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<? echo _RUTA_WEB_NUCLEO; ?>src/css/icon-font.css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" href="<? echo _RUTA_WEB_NUCLEO; ?>src/css/font-sourse-sans.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" href="<? echo _RUTA_WEB_NUCLEO;?>src/css/animate.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<? echo _RUTA_WEB_NUCLEO; ?>src/css/login.adm.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="<? echo _RUTA_WEB_NUCLEO; ?>src/css/login-theme.adm.css" rel="stylesheet" type="text/css">
  <?PHP if (_THEME_DEFAULT){ ?>
  <link rel="stylesheet" href="<? echo _THEME_DEFAULT;?>" rel="stylesheet" type="text/css">
  <?php } ?>
  <?php
    echo $fmt->header->js_jquery();
    echo $fmt->header->titlePage("Olvidaste tu Contraseña?");
    require_once(_RUTA_NUCLEO."modulos/login/forgot.form.php");
  ?>
  <div class="login-footer">
    <?php 
      echo _PIE_PAGINA." "._VZ;  
    ?>
  </div>
  <script type="text/javascript" language="javascript" src="<? echo _RUTA_WEB_NUCLEO; ?>src/js/core.js"></script>
</body>
</html>
