<?PHP
header('Content-Type: text/html; charset=utf-8_spanish_ci');
class SESION
{

	var $fmt;

	function sesion($fmt)
	{
		$this->fmt = $fmt;
	}

	function iniciar_sesion()
	{
		setcookie("COOKIE_CLOSE_NAVEGADOR", TRUE, 0);
		# Session lifetime of 3 hours
		define('DURACION_SESION', '7200'); //2 horas
		ini_set("session.cookie_lifetime", DURACION_SESION);
		ini_set("session.gc_maxlifetime", DURACION_SESION);
		ini_set("session.save_path", "/tmp");
		session_cache_expire(DURACION_SESION);
		session_start();
	}

	function set_nombre_sesion($nombre)
	{
		session_name($nombre);
	}

	function get_nombre()
	{
		return session_name();
	}

	function get_id()
	{
		return session_id();
	}

	function liberar_sesion($nombre)
	{
		session_unset();
		if (isset($nombre)) {
			session_destroy();
		}
	}

	function cerrar_sesion()
	{
		session_destroy();
	}

	function imprimir()
	{
		var_dump($_SESSION);
	}

	function liberar_variable($nombre)
	{
		if (session_is_registered($nombre)) {
			session_unregister($nombre);
		}
	}

	function existe_variable($nombre)
	{
		if (isset($_SESSION[$nombre])) {
			return true;
		} else {
			return false;
		}
	}

	function get_variable($nombre)
	{
		if ($this->existe_variable($nombre)) {
			return $_SESSION[$nombre];
		} else {
			return false;
		}
	}

	function set_variable($nombre, $valor)
	{
		//session_register($nombre);
		$_SESSION[$nombre] = $valor;
	}

	function getVariable($name)
	{
		if ($this->existVar($name)) {
			return $_SESSION[$name];
		} else {
			return false;
		}
	}

	function setVariable($nombre, $valor)
	{
		//session_register($nombre);
		$_SESSION[$nombre] = $valor;
	}

	//v2.1

	function setVar($name, $value)
	{
		//session_register($nombre);
		$_SESSION[$name] = $value;
	}

	function getVar($name)
	{
		if ($this->existVar($name)) {
			return $_SESSION[$name];
		} else {
			return false;
		}
	}

	function existVar($name)
	{
		if (isset($_SESSION[$name])) {
			return true;
		} else {
			return false;
		}
	}
}