<?php
header('Content-Type: text/html; charset=utf-8');

class MODULO{

	var $fmt;

	function __construct($fmt) {
		$this->fmt = $fmt;
	}

	// Modulo

	public function datosId($id){
		$sql ="SELECT * FROM modulo WHERE mod_id='".$id."'";
		$rs = $this->fmt->query->consulta($sql,__METHOD__);
		$num = $this->fmt->query->numRegistros($rs);
		if ($num > 0){
			return $this->fmt->query->obtFila($rs);
		}else{
		  	return 0;
		}
		$this->fmt->query->liberarConsulta($rs);
	}

    public function rutaAmigableModulo($id){
        $sql="select mod_ruta_amigable from modulo where mod_id='".$id."'";
        $rs=$this->fmt->query->consulta($sql,__METHOD__);
        $fila=$this->fmt->query->obtFila($rs);
        return $fila["mod_ruta_amigable"];
    } 

    public function estadoPublicacion($vars){
		$estado = $vars["estado"];
		$id_mod = $vars["id_mod"];
		$disabled = $vars["disabled"];
		$id = $vars["id"]; 

		if(empty($id))
		$id = $vars["item"];		

		if(empty($id_mod))
		$id_mod = $vars["idMod"];
		
		$class = $this->fmt->emptyReturn($vars["class"],'btn-fila-activar');
		$activar = $this->fmt->emptyReturn($vars["activar"],"activar");

		if( $estado==1){
		    $html= "<a title='activo' class='btn ".$class." ".$disabled."' id='btn-p-".$id_mod."-".$id."' id_mod='".$id_mod."' vars='".$activar.",".$id.",0' id_item='".$id."' estado='0' ><span style='display:none'>0</span><i class='icn-eye-open color-text-negro-b'></i></a>";
		}else{
		    $html= "<a title='desactivado' class='btn ".$class." ".$disabled."' id='btn-p-".$id_mod."-".$id."' id_mod='".$id_mod."' id_item='".$id."' estado='1' vars='".$activar.",".$id.",1' ><span style='display:none'>1</span><i class='icn-eye-close color-text-gris-a'></i></a>";
		}

		return $html;
	}

	public function eliminarItemModulo($vars){
		$idMod = $vars["idMod"];
		$idItem = $vars["idItem"];

		$arrayModulo = $this->datosId($idMod);
		$nombreModulo = $arrayModulo["mod_nombre"];
		$aux="";
		$bd = $arrayModulo["mod_bd"];
		$bd_prefijo = $arrayModulo["mod_bd_prefijo"];
		$bd_relaciones = $arrayModulo["mod_bd_relaciones"];


		if (empty($bd) || empty($bd_prefijo) ){
			return "error,<label>El campo Base de Datos o Prefijo estan vacios $nombreModulo </label>";
		}

		if (!$this->fmt->query->existeTabla($bd)){
			return "error,<label>No existe tabla de relaciones en la Base de Datos ".$bd." : ".$nombreModulo."</label>";
		} 

		if (!empty($bd_relaciones)){
			$relaciones = explode(",",$bd_relaciones);
			$c_r = count($relaciones);
			for ($i=0; $i < $c_r; $i++) {
				$filax = explode(":", $relaciones[$i]);
				if (!$this->fmt->query->existeTabla($filax[0])){
					return "error,<label>No existe tabla de relaciones en la Base de Datos ".$filax[0]."-".$nombreModulo."</label>,";
				}else{
					$sql1="DELETE FROM ".$filax[0]." WHERE ".$filax[1]."='".$idItem."'";
					//$aux .= "[ ".$sql1." ]";
					$this->fmt->query->consulta($sql1,__METHOD__);
				}
			}
		}

		if ($bd == "documento"){
			$consulta = "SELECT doc_url FROM documento WHERE doc_id='".$idItem."'";
			$rs =$this->fmt->query->consulta($consulta);
			$row=$this->fmt->query->obtFila($rs);
			$urlArchivo = _RUTA_HOST.$row["doc_url"];
			$this->fmt->archivo->eliminarArchivoUrl($urlArchivo);
		}

		if ($bd == "multimedia"){
			$consulta = "SELECT mul_url_archivo FROM multimedia WHERE mul_id='$idItem'";
			$rs =$this->fmt->query->consulta($consulta);
			$row=$this->fmt->query->obtFila($rs);
			$a_url =$row["mul_url_archivo"];
			$archivo = _RUTA_HOST.$a_url;
			$archivo_thumb = _RUTA_HOST.$this->fmt->archivo->urlAdd($a_url,"-thumb");
			$archivo_mini = _RUTA_HOST.$this->fmt->archivo->urlAdd($a_url,"-mini");
			$archivo_web = _RUTA_HOST.$this->fmt->archivo->urlAdd($a_url,"-web");
			$archivo_medium = _RUTA_HOST.$this->fmt->archivo->urlAdd($a_url,"-medium");
			$this->fmt->archivo->eliminarArchivoUrl($archivo);
			$this->fmt->archivo->eliminarArchivoUrl($archivo_thumb);
			$this->fmt->archivo->eliminarArchivoUrl($archivo_mini);
			$this->fmt->archivo->eliminarArchivoUrl($archivo_web);
			$this->fmt->archivo->eliminarArchivoUrl($archivo_medium);
		}

		$sqle="DELETE FROM ".$bd." WHERE ".$bd_prefijo."id='".$idItem."'";
		$this->fmt->query->consulta($sqle,__METHOD__);
		// $aux .= "[ ".$sqle." ]";

		$up_sqr6 = "ALTER TABLE ".$bd." AUTO_INCREMENT=1";
		$this->fmt->query->consulta($up_sqr6,__METHOD__);
		// $aux .= "[ ".$up_sqr6." ]";

		return "realizado,".$idItem.",".$aux; 
	}

	// Acciones js 
	
	public function scriptPage($vars){
      $id_mod = $vars["id_mod"];
      $idMod = $vars["idMod"];

      $cadena = '';
      $cadena .='<!-- inicio de scriptPage(); -->'; 
      $cadena .='	<script language="JavaScript">';
      $cadena .=' 		var id_mod="'.$id_mod.'";';
      $cadena .=' 		var idMod="'.$idMod.'";';
      $cadena .=' 		var sitio="'._RUTA_WEB.'";';      
      $cadena .='	</script>';
      $cadena .='	<link rel="stylesheet" href="'._RUTA_WEB_NUCLEO.'src/css/render.css" rel="stylesheet" type="text/css">';
      $cadena .='	<script language="JavaScript" src="'._RUTA_WEB_NUCLEO.'src/components/render.js?'.$this->fmt->randNum().'"></script>';
      $cadena .='	<script language="JavaScript" src="'._RUTA_WEB_NUCLEO.'src/components/scriptPage.js?'.$this->fmt->randNum().'"></script>';
      $cadena .='<!-- fin de scriptPage(); -->'; 

      return $cadena;
    }

    public function scriptTable($vars){

    	$html = '';
    	$id = $vars["id"];
    	$idMod = $vars["idMod"];
    	$tipo = $this->fmt->emptyReturn($vars["tipo"],"desc");
    	$orden = $this->fmt->emptyReturn($vars["orden"],"0");
    	$cant = $this->fmt->emptyReturn($vars["cantidad"],"25");
    	$pageUp = $this->fmt->emptyReturn($vars["pagUp"],true);

    	//function script_table($id,$id_mod,$tipo="asc",$orden="0",$cant=25,$pag_up=fals){
    	$html .='<!-- Inicio de scriptTable(); -->';
      	$html .='<script type="text/javascript" language="javascript" src="'._RUTA_WEB_NUCLEO.'src/js/jquery.dataTables.min.js"></script>';
      	$html .='<link rel="stylesheet" href="'._RUTA_WEB_NUCLEO.'src/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">';
        $html .='<link rel="stylesheet" href="'._RUTA_WEB_NUCLEO.'src/css/datatables.css" rel="stylesheet" type="text/css">';
        $html .='<link rel="stylesheet" href="'._RUTA_WEB_NUCLEO.'src/css/datatables-theme.css" rel="stylesheet" type="text/css">';
      	
      	$html .='<script type="text/javascript">';
        $html .='	$(document).ready( function() {';
        $html .= ' 		$("#'.$id.'").DataTable({ ';

        if($pageUp){
        	$html .= '    	"dom": \'<"top"flp<"clear">>rt<"bottom"ip<"clear">>\',';
        }
    	$html .= '    		"language": {
				            "sProcessing":     "Procesando...",
				            "sLengthMenu":     "Mostrar _MENU_ registros",
				            "sZeroRecords":    "No se encontraron resultados",
				            "sEmptyTable":     "Ningún dato disponible en esta tabla",
				            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				            "sInfoPostFix":    "",
				            "sSearch":         "<i class=\'icn icn-search\'></i>",
				            "sUrl":            "",
				            "sInfoThousands":  ",",
				            "sLoadingRecords": "Cargando...",
				            "oPaginate": {
				            "sFirst":    "Primero",
				            "sLast":     "Último",
				            "sNext":     "<i class=\'btn btn-small icn-chevron-right\'></i>",
				            "sPrevious": "<i class=\'btn btn-small icn-chevron-left\'></i>"
				            },
				            "oAria": {
				            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
				            }
				            },
				            "bSortable": true,
				            "pageLength": '.$cant.',';
		if ($orden!=""){ 
			$html .= '		"order": [[ '.$orden.', \''.$tipo.'\' ]] ';
        } 
        $html .= '		});'; 
        $html .= '});'; 
        $html .= '</script>'; 
        $html .= '<!-- fin de scriptTable(); -->'; 

        return $html;
    }

  	// Codigos

	public function crearCodigo($vars){
		$prefijo = $vars["prefijo"];
		$cantidad  = $vars["cantidad"];
		$from  = $vars["from"];
		$row  = $vars["row"];

	    //$pattern = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	    for($i = 0; $i < $cant; $i++) {

	        $key .= $pattern{rand(0, $cantidad)};

	    }
	    if (!empty($prefijo)){
	    	$codigo = $prefijo."".$key;
	    }

	    if (!empty($from) || !empty($row) ){
	    	if(codigoRepetido($codigo,$from,$row)){
		      crearCodigo();
		    }else{
		      return $codigo;
		    }
	    }else{
	    	return $codigo;
	    } 
	}

	public function codigoRepetido($codigo,$from,$row){
	  $consulta = "SELECT * FROM ".$from." WHERE ".$row."='".$codigo."'";
	  $rs =$this->fmt->query->consulta($consulta);
	  $num=$this->fmt->query->num_registros($rs);
	  if($num>0){
	    return true;
	  }else{
	    return false;
	  }
	  $fmt->query->liberar_consulta();
	}

	// Editor de texto

	public  function modalEditorTexto($id,$height="430"){
		if($id[0]=="."){
        	$idx=$id;
      	}else{
        	$idx ="#".$id;
      	}

		return "<script>
					
						var adicionarImagen = function (context) {
					    var ui = $.summernote.ui;

					    // create button
					    var button = ui.button({
					      contents: '<i class=note-icon-picture/>',
					      tooltip: 'imagen',
					      click: function () {
					         $('".$idx."').summernote('editor.saveRange');
					         $('.modal-finder').appendTo('body');
					         $('.modal-finder').addClass('on');
					         insert = '".$idx."';
					         var upload = 'insertar-editor-texto';
					         var vars= '';
					         var seleccion = 'simple';
					         cargar_finder(insert,upload,vars,seleccion);
					         //context.invoke('editor.insertText', window.globalVar );
					      }
					    });

					    return button.render();   // return button as jquery object
					  }

					  $('".$idx."').summernote({
					      height: ".$height.",                 // set editor height
					      minHeight: null,             // set minimum height of editor
					      maxHeight: null,             // set maximum height of editor
					      lang: 'es-ES',
					      paragraph: 'justifyLeft',
					      focus: false,
					      styleTags:['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', { title: 'imagen-left', tag: 'imagen-left', className: 'imagen-left', value: '' } ],
					      toolbar: [
					          ['style', ['style','bold', 'italic' ]],
					          ['color', ['color']],
					          ['para', ['ul', 'ol', 'paragraph','table']],
					          ['fontsize', ['fontsize']],
					          ['codeview',['clear','codeview','fullscreen']],
					          ['mybutton', ['hello','link','imagen','video']],
					        ],
					        onInit : function(){
					          $('.note-btn').attr('title', '');
					        },
					        buttons: {
					          imagen: adicionarImagen
					        }
					  });
					  $('.note-btn').attr('title', '');
				</script>";
    }

    //numeros

    public function esPrimo($num){
		if ($num == 2 || $num == 3 || $num == 5 || $num == 7) {
	        return True;
	    } else {
	        // comprobamos si es par
	        if ($num % 2 != 0) {
	            // comprobamos solo por los impares
	            for ($i = 3; $i <= sqrt($num); $i += 2) {
	                if ($num % $i == 0) {
	                    return False;
	                }
	            }
	            return True;
	        }
	    }
    }

    // Conversiones String y textos 

    public function cadenaArrayVars($cadena){
    	$cadena = "'".str_replace(":","'=>'",$cadena)."'";
		$cadena = "array(".str_replace(",","','",$cadena).")";
		$array = eval("return " .$cadena. ";");
		return $array;
    }

    public function stringToUrl($cadena){
	  	$cadena= utf8_decode($cadena);
	  	$cadena = strtolower($cadena);
	    $cadena = str_replace(' ', '-', $cadena);
	  	$cadena = str_replace('?', '', $cadena);
	  	$cadena = str_replace('+', '', $cadena);
	  	$cadena = str_replace(':', '', $cadena);
	  	$cadena = str_replace('??', '', $cadena);
	    $cadena = str_replace('`', '', $cadena);
	  	$cadena = str_replace('\'', '', $cadena);
	  	$cadena = str_replace('!', '', $cadena);
	  	$cadena = str_replace('¿', '', $cadena);
	    $cadena = str_replace(',', '-', $cadena);
	    $cadena = str_replace('(', '', $cadena);
	    $cadena = str_replace(')', '', $cadena);
	  	$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ??';
	    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
	    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);

	    return $cadena;
  	}

    public function recortarTexto($texto,$cantidad="0"){
	    $cn=$this->cantidadCaracteres($texto);
	    if($cn>$cantidad){
	        $aux="...";
	    }else{
	        $aux="";
	    }
	    return substr($texto, 0, $cantidad).$aux;
    }

    public function cantidadCaracteres($texto){
        return strlen($texto);
    }

    public function redireccionar($ruta,$tiempo){
    	$html ='';
    	$html .='<script type="text/javascript">';
        $html .='  setTimeout(function() {';
        $html .='      $(".modal-form").html("");';
        $html .='      $(".modal-form .modal-inner").html("");';
        $html .='      $(".body-page").css("overflow-y","auto");';
        $html .='      document.location.href="'.$ruta.'";';
        $html .='  }, '.$tiempo.');';
        $html .='</script>';

        return $html;   	
    }

    // Fechas y horas

    public function dateArray($zona){
		setlocale(LC_TIME,"es_ES");
		date_default_timezone_set($zona);
		
		$dateArray = explode(",", date("Y,m,d,H,i,s"));
		$dat["Y"] = $dateArray[0];
		$dat["m"] = $dateArray[1];
		$dat["d"] = $dateArray[2];
		$dat["H"] = $dateArray[3];
		$dat["i"] = $dateArray[4];
		$dat["s"] = $dateArray[5];

		return $dat;
	}  

    function estructurarFechaHora($fecha,$format='dd-mm-yyyy hh:ii'){
		$fechaHora = explode(" ", $fecha);
		$fechas = explode("-", $fechaHora[0]);
		$tiempo = explode (":", $fechaHora[1]);


		$ano=$fechas[0];
		$mes=$fechas[1];
		$dia=$fechas[2];
		$hora = $tiempo[0];
		$min = $tiempo[1];
		$seg = substr($tiempo[2], 0, 2);

		if ($format=="dd-mm-yyyy hh:ii"){
		return $dia."-".$mes."-".$ano." ".$hora.":".$min;
		}      
		if ($format=="dd-mm-yyyy"){
		return $dia."-".$mes."-".$ano;
		}
    }

    function desestructurarFechaHora($fecha){
		$fechaHora = explode(" ", $fecha);
		$fechas = explode("-", $fechaHora[0]);
		$tiempo = explode (":", $fechaHora[1]);
		$ano=$fechas[2];
		$mes=(string)(int)$fechas[1];
		$dia=$fechas[0];
		$hora = $tiempo[0];
		$min = $tiempo[1];
		$seg = substr($tiempo[2], 0, 2);
		return $ano."-".$mes."-".$dia." ".$hora.":".$min.":00";
    }

    public function dateFormat($zona="America/La_Paz",$formato="Y-m-d H:i:s",$setlocale="es_ES"){
        setlocale(LC_TIME,$setlocale);
        date_default_timezone_set($zona);
        return date($formato);
	}

	public function formatDate($vars){
		$fecha = $vars["fecha"];
		$modo = $this->fmt->emptyReturn($vars["modo"],"normal");
		$formato = $this->fmt->emptyReturn($vars["formato"],"d,m,a,h,mi,s");
		$numDiaLiteral= $this->dayLiteralNum($fecha);
		$fechaHora = explode(" ", $fecha);
		$fechas = explode("-", $fechaHora[0]);
		$tiempo = explode (":", $fechaHora[1]);
		$ano=$fechas[0];
		$mes=(string)(int)$fechas[1];
		$dia=$fechas[2];
		$hora = $tiempo[0];
		$min = $tiempo[1];
		$seg = substr($tiempo[2], 0, 2);

		if ($modo=="mini"){
			$day = array(' ','Lun','Mar','Mie','Jue','Vie','Sab','Dom');
			$month = array(' ','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
		}      
		if ($modo=="normal" || $modo="lineal"){
			$day = array(' ','Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo');
			$month = array(' ','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
		}		

		if ($modo=="numeral"){
			$day = array(' ','01','02','03','04','05','06','07');
			$month = array(' ','01','02','03','04','05','06','07','08','09','10','11','12');
		}

		$fx = explode(",",$formato);
		$nfx = count($fx);

		$F .= "<div class='block-date'>";

		$dl = "<span class='diaLiteral'>".$day[$numDiaLiteral]." </span>";
		$d = " <span class='dia'>".$dia." </span>";
		$m = " <span class='mes'>".$month[$mes]." </span>";
		$a = " <span class='ano'>".$ano." </span>";
		$h = " <span class='hora'>".$hora."</span>";
		$mi = " <span class='min'>".$min."</span>";
		$s = " <span class='seg'>".$seg."</span>";

		for ($j=0; $j < $nfx ; $j++) { 
		if ($fx[$j]=="dl"){ $F .= $dl;  }
		if ($fx[$j]=="d"){ $F .= $d;  }
		if ($fx[$j]=="m"){ $F .= $m;  }
		if ($fx[$j]=="a"){ $F .= $a;  }
		if ($fx[$j]=="h"){ $F .= $h;  }
		if ($fx[$j]=="mi"){ $F .= $mi;  }
		if ($fx[$j]=="s"){ $F .= $s;  }
		}

		$F .= "</div>";

		if ($formato=="d/m/a"){
			$F ="";
			if($modo="numeral"){
				$F .= $dia."/".$mes."/".$ano;
			}else{
				$F .= "<div class='block-date'>";
 				$F .= $dia."/".$mes."/".$ano;
				$F .= "</div>";
			}
		}

		if ($modo = "lineal"){
			$F = $dia." de ".$month[$mes]." de ".$ano; 
		}

		return $F;
	}

	public function dayLiteralNum($date){
		return strftime("%u", strtotime($date));
	}

	public function dayConverter($vars){
		$day = $vars["day"];
		$type = $vars["type"];
		$lang = $vars["lang"];

		switch ($lang) {
			case 'es':
				$dayMin = array(' ','lun','mar','mie','jue','vie','sab',"dom");
				$dayNormal = array(' ','Lunes','Martes','Miercoles','Jueves','Viernes','S&aacute;bado',"Domingo");
			break;
		}
		
		switch ($type) {
			case 'normal':
				for ($i=0; $i < 8; $i++) { 
					if ($dayMin[$i] == $day){
						return $dayNormal[$i];
					} 
				}
			break;			
			
			case 'min':
				for ($i=0; $i < 8; $i++) { 
					if ($dayNormal[$i] == $day){
						return $dayMin[$i];
					} 
				}
			break;
			
			case 'numeral,min':
				return $dayMin[$day];
			break;

			case 'numeral,normal':
				return $dayNormal[$day];
			break;
		}
	}  

	public function dateCompare($date1,$signalCompare="=",$date2){

		$date1 =  strtotime($date1);
		$date2 =  strtotime($date2);

		if ($signalCompare == ">"){
			if($date1 > $date2){
				return true;
			}else{
				return false;
			}
		}		
		if ($signalCompare == "<"){
			if($date1 < $date2){
				return true;
			}else{
				return false;
			}
		}

		if ($signalCompare == "="){
			if($date1 == $date2){
				return true;
			}else{
				return false;
			}
		}
	}

	public function tiempoRestante($vars){
		$desde = $vars["fecha1"];
		$hasta = $vars["fecha2"];
		$modo = $this->fmt->emptyReturn($vars["modo"],'normal');

          $ini = explode(" ",$desde);
          $fIni = $ini[0];
          $hIni = $ini[1];
          $fIni = explode("-",$fIni);
          $hIni = explode(":",$hIni);

          $fin = explode(" ",$hasta);
          $fFin = $fin[0];
          $hFin = $fin[1];
          $fFin = explode("-",$fFin);
          $hFin = explode(":",$hFin);

          $anos = $fFin[0] - $fIni[0];
          $meses = $fFin[1] - $fIni[1];
          $dias = $fFin[2] - $fIni[2];

          $horas = $hFin[0] - $hIni[0];
          $minutos = $hFin[1] - $hIni[1];
          $segundos = $hFin[2] - $hIni[2];

          if ($segundos < 0) {
              $minutos--;
              $segundos = 60 + $segundos;
          }
          if ($minutos < 0) {
              $horas--;
              $minutos = 60 + $minutos;
          }

          if ($horas < 0) {
              if ($dias!=0){
                //$dias--;
                $horas = 24 + $horas;
              }
          }

          //echo "d:".$dias;

          if ($dias << 0){
            //echo "ingrese";
            if ($meses!=0){
              //$meses--;

              switch ($fIni[1]) {
                  case 1:     $dias_mes_anterior=30; break;
                  case 2:
                              if (checkdate(2,29,$fIni[0])){
                                  $dias_mes_anterior=29; break;
                              } else {
                                  $dias_mes_anterior=28; break;
                              }
                  case 3:     $dias_mes_anterior=31; break;
                  case 4:     $dias_mes_anterior=31; break;
                  case 5:     $dias_mes_anterior=30; break;
                  case 6:     $dias_mes_anterior=31; break;
                  case 7:     $dias_mes_anterior=30; break;
                  case 8:     $dias_mes_anterior=31; break;
                  case 9:     $dias_mes_anterior=31; break;
                  case 10:     $dias_mes_anterior=30; break;
                  case 11:     $dias_mes_anterior=31; break;
                  case 12:     $dias_mes_anterior=30; break;
              }
            }
          }

          if ($meses < 0){
              --$anos;
              $meses = $meses + 12;
          }

          if($anos==0){
            if($meses==0){
              if($dias==0){
                if($horas==0){
                  if($minutos==0){
                    if ($modo=="normal"){
                      $tiempo="Hace instantes";
                    }
                    if ($modo=="mini"){
                      $tiempo="5s";
                    }
                  }else{
                    if ($modo=="normal"){
                      $tiempo="Hace ".$minutos."min.";
                    }
                    if ($modo=="mini"){
                      $tiempo= $minutos."m.";
                    }
                  } // fin min
                }else{
                  if ($horas<0){
                    $tiempo .=$this->fmt->mensaje->programado();
                    $tiempo .=$this->formatDate(array('fecha' => $desde));
                  }else{
                    if ($modo=="normal"){
                      $tiempo="Hace ".$horas."hr.";
                    }
                    if ($modo=="mini"){
                      $tiempo= $horas."h.";
                    }
                  }
                }// fin horas
              }else{
                if ($dias<0){
                  // menus de un día
                  $tiempo .=$this->fmt->mensaje->programado();
                  $tiempo .=$this->formatDate(array('fecha' => $desde));
                }else{
                  // más de 1 día
                  if($dias==1){
                    $tiempo="Ayer";
                  }else{
                    if($dias>7){
                      $tiempo=$this->formatDate(array('fecha' => $desde));

                      if ($modo=="mini"){
                      	$tiempo =$this->formatDate(array('fecha' => $desde,'formato' => 'd,m,a','modo' =>'mini'));
                      }
                    }else{
                      if ($modo=="normal"){
                        $tiempo="Hace ".$dias." días.";
                      }
                      if ($modo=="mini"){
                        $tiempo= $dias."d.";
                      }
                    }
                  }
                }
              } // fin +7 días
            }else{
              $d = $dias + $dias_mes_anterior;
              if($d<=7){
                if ($modo=="normal"){
                  $tiempo="Hace ".$d." días.";
                }
                if ($modo=="mini"){
                  $tiempo= $d."d.";
                }
              }else{
                //echo $meses;
                if ($meses<0){
                  $tiempo =$this->fmt->mensaje->programado();
                  $tiempo .=$this->formatDate(array('fecha' => $desde));
                  if ($modo=="mini"){
                    $tiempo =$this->fmt->mensaje->programado();
                    $tiempo .=$this->formatDate(array('fecha' => $desde,'formato' => 'd,m,a','modo' =>'mini'));
                    //$tiempo .="hace menos de 1 mes";
                  }
                }else{
                  $tiempo =$this->formatDate(array('fecha' => $desde));
                  if ($modo=="mini"){
                    $tiempo =$this->formatDate(array('fecha' => $desde,'formato' => 'd,m,a','modo' =>'mini'));
                    //$tiempo .="hace mas de 1 mes";
                  }
                }
              }
            }// fin meses
          }else{
          	if ($modo=="normal"){
            	$tiempo=$this->formatDate(array('fecha' => $desde));
        	}
            if ($modo=="mini"){
                    $tiempo =$this->formatDate(array('fecha' => $desde,'formato' => 'd,m,a', 'modo' =>'mini'));
                    //$tiempo .="hace";
            }
          } // fin años
        return $tiempo;
	}

 

} 