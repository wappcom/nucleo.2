<?php
header('Content-Type: text/html; charset=utf-8');

class VALIDAR{

	var $fmt;

	function __construct($fmt){
		$this->fmt = $fmt;
	}

	public function nombreCompleto($texto){
		$arrayNom = explode(" ", $texto);
		$contaNom = count($arrayNom);
		$numCaracteres = strlen($texto);
		$retorno["estado"] ="valido";

		if (empty($texto)){
			$retorno["estado"] ="invalido";
			$retorno["error"] ="vacio";
			return $retorno;
		}

		if($contaNom <= 1){
			$retorno["estado"] ="invalido";
			$retorno["error"] ="invalido";
			return $retorno;
		}		

		if($numCaracteres < 5){
			$retorno["estado"] ="invalido";
			$retorno["error"] ="invalido";
			return $retorno;
		}

		return  $retorno;
	}	

	public function nombre($texto){

		$numCaracteres = strlen($texto);
		$retorno["estado"] ="valido";

		if (empty($texto)){
			$retorno["estado"] ="invalido";
			$retorno["error"] ="vacio";
			return $retorno;
		}	

		if($numCaracteres <= 2){
			$retorno["estado"] ="invalido";
			$retorno["error"] ="invalido";
			return $retorno;
		}

		return  $retorno;
	}

	public function email($email){
		$retorno["estado"] ="valido";

		if (empty($email)){
		  	$retorno["estado"] ="invalido";
			$retorno["error"] ="vacio";
			return $retorno;
		}else{
		  if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE){
		    $retorno["estado"] ="invalido";
			$retorno["error"] ="invalido";
			return $retorno;
		  }
		}

		return $retorno;
	}
}