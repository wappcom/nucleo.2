<?php
header('Content-Type: text/html; charset=utf-8');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MAIL
{
    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;

        // Cargar las clases de PHPMailer
        /* require_once (_RUTA_NUCLEO . "vendor/phpmailer/src/Exception.php");
        require_once (_RUTA_NUCLEO . "vendor/phpmailer/src/PHPMailer.php");
        require_once (_RUTA_NUCLEO . "vendor/phpmailer/src/SMTP.php"); */
    }

    function traer_smtp()
    {
        $row["smtp"] = _SMTP;
        $row["port"] = _PUERTO;
        $row["salida_correo"] = _CORREO;
        $row["contracena_sa"] = _PASSWORD_MAIL;
        $row["entrada_correo"] = _CORREO;
        $row["seguridad_mail"] = _SEGURIDAD_MAIL;
        return $row;
    }

    function enviar($correo, $nombre_c, $mensaje, $asunto, $nombre, $seguridad = "tls")
    {

        $rw = $this->traer_smtp();

        $mail = new PHPMailer(true);

        try {
            $mail->SetLanguage('es');
            $mail->isSMTP();                                     // Set mailer to use SMTP
            $mail->Host = $rw['smtp'];  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $rw['salida_correo'];                 // SMTP username
            $mail->Password = $rw['contracena_sa'];                           // SMTP password
            $mail->SMTPSecure = $rw['seguridad_mail'];                               // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $rw['port'];                                    // TCP port to connect to

            $mail->setFrom($rw['entrada_correo'], utf8_decode($nombre));

            $mail->addAddress($correo, $nombre_c);     // Add a recipient

            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = utf8_decode($asunto);
            $mail->Body = utf8_decode($mensaje);
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function send($vars)
    {
        $email = $vars["email"];
        $name = $vars["name"];
        $subject = $vars["subject"];
        $setFromMail = $vars["setFromMail"];
        $setFromName = $vars["setFromName"];
        $body = $vars["body"];
        $eUser = $vars["confirmReadingTo"];

        require (_RUTA_NUCLEO . "vendor/mailerphp/autoload.php");

        $mail = new PHPMailer;
        //$mail->isSMTP = 1;
        //$mail->SMTPAuth = 1;

        try {
            //Server settings
            $mail->SMTPDebug = 0;                      //Enable verbose debug output
            $mail->isSMTP();
            $mail->SMTPAuth = 1;
            //$mail->isSMTP = 1;                                      //Send using SMTP
            $mail->Host = "cenaceupsa.com.bo";                     //Set the SMTP server to send through                 //Enable SMTP authentication
            $mail->Username = "mailsend@cenaceupsa.com.bo";                     //SMTP username
            $mail->Password = "Cenace123A!";                               //SMTP password
            $mail->SMTPSecure = 'ssl';         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port = 465;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom('mailsend@cenaceupsa.com.bo', $setFromName);
            $mail->addAddress($email, $name);     //Add a recipient

            //Content
            $mail->isHTML(true);                                //Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;

            //Custom Headers
            /* $mail->addCustomHeader("X-Confirm-Reading-To: $eUser");
            $mail->addCustomHeader("Return-Receipt-To: $eUser");
            $mail->addCustomHeader("Disposition-Notification-To: $eUser"); */
            $mail->ConfirmReadingTo = $eUser;

            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $send = $mail->send();
            return $send;
        } catch (Exception $e) {
            return "error, {$mail->ErrorInfo}";
        }
    }
}
