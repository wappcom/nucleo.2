<?PHP
header("Content-Type: text/html;charset=utf-8");

class CONTENIDO
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    public function datos_id($id)
    {
        $consulta = "SELECT * FROM  contenido WHERE conte_id='$id'";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->num_registros($rs);
        if ($num > 0) {
            $row = $this->fmt->query->obt_fila($rs);
            return $row;
        } else {
            return 0;
        }
        $this->fmt->query->liberar_consulta();
    }

    public function datosId($id)
    {
        $consulta = "SELECT * FROM  contenido WHERE conte_id='$id'";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->num_registros($rs);
        if ($num > 0) {
            $row = $this->fmt->query->obt_fila($rs);
            return $row;
        } else {
            return 0;
        }
        $this->fmt->query->liberar_consulta();
    }


    public function cuerpo($id)
    {
        $consulta = "SELECT conte_cuerpo FROM  contenido WHERE conte_id='$id'";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->num_registros($rs);
        if ($num > 0) {
            $row = $this->fmt->query->obt_fila($rs);
            return  strip_tags($row["conte_cuerpo"]);
        } else {
            return 0;
        }
        $this->fmt->query->liberar_consulta();
    }

    public function json_cuerpo($id)
    {
        $consulta = "SELECT conte_cuerpo FROM  contenido WHERE conte_id='$id'";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->num_registros($rs);
        if ($num > 0) {
            $row = $this->fmt->query->obt_fila($rs);
            return json_decode(strip_tags($row["conte_cuerpo"]));
        } else {
            return 0;
        }
        $this->fmt->query->liberar_consulta();
    }

    public function relacionCategorias($id)
    {
        $consulta = "SELECT DISTINCT cat_id,cat_ruta_amigable FROM  categoria, contenido_categorias WHERE conte_cat_cat_id=cat_id  and conte_cat_conte_id='$id'  ORDER BY `contenido_categorias`.`conte_cat_orden` desc";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->num_registros($rs);
        if ($num > 0) {
            // for($j=0;$j<$num;$j++){
            $row_conte = $this->fmt->query->obt_fila($rs);
            return $row_conte;
            // }
        } else {
            return 0;
        }
        $this->fmt->query->liberar_consulta();
    }

    public function relations($id=0)
    {
        $consulta = "SELECT DISTINCT cat_id,cat_ruta_amigable, conte_cat_orden FROM  categoria, contenido_categorias WHERE conte_cat_cat_id=cat_id  and conte_cat_conte_id='$id'  ORDER BY `contenido_categorias`.`conte_cat_orden` desc";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->num_registros($rs);
        if ($num > 0) {
            for($j=0;$j<$num;$j++){
                $row_conte = $this->fmt->query->obt_fila($rs);
                $rtn[$j]["id"] = $row_conte["cat_id"];
                $rtn[$j]["path"] = $row_conte["cat_ruta_amigable"];
                $rtn[$j]["orden"] = $row_conte["conte_cat_orden"];
            }
            return $rtn;
        } else {
            return 0;
        }
        $this->fmt->query->liberar_consulta();
    }

    public function contenidoMultimedia($id)
    {
        $consulta = "SELECT DISTINCT mul_id FROM  contenido, contenido_multimedia,multimedia WHERE conte_mul_mul_id=mul_id  and conte_mul_conte_id='$id'  ORDER BY `contenido_multimedia`.`conte_mul_orden` desc";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->numRegistros($rs);
        if ($num > 0) {
            for ($j = 0; $j < $num; $j++) {
                $row_conte[$j] = $this->fmt->query->obtFila($rs);
            }
            return $row_conte;
        } else {
            return 0;
        }
        $this->fmt->query->liberarConsulta();
    }


    public function lastContentCatDate($idCat)
    {
        $consulta = "SELECT DISTINCT conte_id,conte_titulo,conte_fecha,conte_ruta_amigable,conte_subtitulo,conte_cuerpo,conte_foto FROM  contenido, contenido_categorias WHERE conte_activar=1 AND conte_cat_cat_id='" . $idCat . "'  and conte_cat_conte_id=conte_id  ORDER BY `contenido`.`conte_fecha` desc";
        $rs = $this->fmt->query->consulta($consulta);
        $num = $this->fmt->query->num_registros($rs);
        if ($num > 0) {
            // for($j=0;$j<$num;$j++){
            $row_conte = $this->fmt->query->obt_fila($rs);
            return $row_conte;
            // }
        } else {
            return 0;
        }
        $this->fmt->query->liberar_consulta();
    }

    public function apostrofe($str)
    {
        $str = str_replace("'", "\'", $str);
        $str = str_replace('"', '\"', $str);
        return $str;
    }

    public function dataId($id)
    {
        $sql = "SELECT * FROM contenido WHERE conte_id='" . $id . "'";
        $rs = $this->fmt->query->consult($sql, __METHOD__);
        $num = $this->fmt->query->num($rs);
        if ($num > 0) {
            $row = $this->fmt->query->row($rs);
            $return["cont_id"] = $row["conte_id"];
            $return["cont_title"] = $row["conte_titulo"];
            $return["cont_path_url"] = $row["conte_ruta_amigable"];
            $return["cont_subtitle"] = $row["conte_subtitulo"];
            $return["cont_body"] = $row["conte_cuerpo"];
            $return["cont_img"] = $row["conte_foto"];
            $return["cont_date"] = $row["conte_fecha"];
            $return["cont_usu_id"] = $row["conte_id_usuario"];
            $return["cont_tag"] = $row["conte_tag"];
            $return["cont_state_title"] = $row["conte_activar_titulo"];
            $return["cont_state"] = $row["conte_activar"];
            return  $return;
        } else {
            return 0;
        }
        $this->fmt->query->leave();
    }

    public function dataIdCat($idCat)
    {
        $sql = "SELECT DISTINCT conte_id,conte_titulo,conte_ruta_amigable,conte_subtitulo,conte_cuerpo,conte_foto,conte_fecha,conte_id_usuario,conte_tag,conte_clase,conte_activar_titulo,conte_activar FROM contenido, contenido_categorias,categoria WHERE conte_cat_conte_id=conte_id AND conte_cat_cat_id='" . $idCat . "' ORDER BY conte_cat_orden desc";
        $rs = $this->fmt->query->consult($sql);
        $num = $this->fmt->query->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->query->row($rs);
                $return[$i]["cont_id"] = $row["conte_id"];
                $return[$i]["cont_title"] = $row["conte_titulo"];
                $return[$i]["cont_path_url"] = $row["conte_ruta_amigable"];
                $return[$i]["cont_subtitle"] = $row["conte_subtitulo"];
                $return[$i]["cont_body"] = $row["conte_cuerpo"];
                $return[$i]["cont_img"] = $row["conte_foto"];
                $return[$i]["cont_date"] = $row["conte_fecha"];
                $return[$i]["cont_usu_id"] = $row["conte_id_usuario"];
                $return[$i]["cont_tag"] = $row["conte_tag"];
                $return[$i]["cont_state_title"] = $row["conte_activar_titulo"];
                $return[$i]["cont_state"] = $row["conte_activar"];
            }
            return  $return;
        } else {
            return 0;
        }
        $this->fmt->query->leave();
    }

    //JSON
    public function jsonDataId($id)
    {
        $sql = "SELECT * FROM contenido WHERE conte_id='" . $id . "'";
        $rs = $this->fmt->query->consult($sql, __METHOD__);
        $num = $this->fmt->query->num($rs);
        if ($num > 0) {
            $row = $this->fmt->query->row($rs);

            return json_encode($row);
        } else {
            return 0;
        }
        $this->fmt->query->leave();
    }

    public function jsonDataIdCat($idCat)
    {
        $sql = "SELECT DISTINCT conte_id,conte_titulo,conte_ruta_amigable,conte_subtitulo,conte_cuerpo,conte_foto,conte_fecha,conte_id_usuario,conte_tag,conte_clase,conte_activar_titulo,conte_activar FROM contenido, contenido_categorias,categoria WHERE conte_cat_conte_id=conte_id AND conte_cat_cat_id='" . $idCat . "' ORDER BY conte_cat_orden desc";
        $rs = $this->fmt->query->consult($sql);
        $num = $this->fmt->query->num($rs);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $row = $this->fmt->query->row($rs);
                $return[$i]["cont_id"] = $row["conte_id"];
                $return[$i]["cont_title"] = $row["conte_titulo"];
                $return[$i]["cont_path_url"] = $row["conte_ruta_amigable"];
                $return[$i]["cont_subtitle"] = $row["conte_subtitulo"];
                $return[$i]["cont_body"] = $row["conte_cuerpo"];
                $return[$i]["cont_img"] = $row["conte_foto"];
                $return[$i]["cont_date"] = $row["conte_fecha"];
                $return[$i]["cont_usu_id"] = $row["conte_id_usuario"];
                $return[$i]["cont_tag"] = $row["conte_tag"];
                $return[$i]["cont_state_title"] = $row["conte_activar_titulo"];
                $return[$i]["cont_state"] = $row["conte_activar"];
            }
            return json_encode($return);
        } else {
            return 0;
        }
        $this->fmt->query->leave();
    }

    public function limpiar_cadena_word($var)
    {

        $var = str_replace('style="margin-bottom:8px;list-style-image:url(&quot;../images/check.png&quot;);line-height:18px;"', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(&quot;../images/check_round.jpg&quot;); line-height: 18px;"', '', $var);
        $var = str_replace('style="color:rgb(73,169,207);"', '', $var);
        $var = str_replace('class="MsoNormal"', '', $var);
        $var = str_replace('<o:p></o:p>', '', $var);
        $var = str_replace('·&nbsp; &nbsp;', '', $var);
        $var = str_replace('<p <', '<', $var);
        $var = str_replace('<b <', '<', $var);
        $var = str_replace("·&nbsp;", "", $var);
        $var = str_replace("&nbsp;", "", $var);
        $var = str_replace('<strong <', '<', $var);
        $var = str_replace('<ul <', '<', $var);
        $var = str_replace('<li <', '<', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"', '', $var);
        $var = str_replace('style="margin: 1.2pt 25.25pt 0.0001pt 0.6pt;"', '', $var);
        $var = str_replace(`style="margin-top:.55pt;margin-right:194.45pt;margin-bottom:0cm;margin-left:.6pt;margin-bottom:.0001pt;line-height:23.0pt;mso-line-height-rule:exactly"`, '', $var);
        $var = str_replace('style="" calibri","sans-serif";mso-fareast-font-family:="" calibri;color:#006fc0;mso-ansi-language:es-bo"=""', '', $var);
        $var = str_replace('style="margin-top:.55pt;margin-right:194.45pt;margin-bottom:0cm;margin-left:.6pt;margin-bottom:.0001pt;line-height:23.0pt;mso-line-height-rule:exactly"', '', $var);
        $var = str_replace('style="margin-bottom: 7px; color: rgb(52, 51, 60); text-align: justify;"', '', $var);
        $var = str_replace('style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal"', '', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="margin-bottom: 0.0001pt; line-height: normal;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 418.672px; float: right;"', '', $var);
        $var = str_replace('style="width: 418.672px; display: inline-block; margin-bottom: 10px;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 536.766px; float: left; text-align: justify; padding: 10px;"', '', $var);
        $var = str_replace('style="line-height: 0px;"', '', $var);
        $var = str_replace('style="color: white; text-align: center;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify; width: 714px;"', '', $var);
        $var = str_replace('style="line-height: 0px; margin-top: 20px;"', '', $var);
        $var = str_replace('style="text-align:justify;color:rgb(51,51,51);margin-bottom:0px;font-family:calibri;"', '', $var);
        $var = str_replace('style="color:rgb(73,169,207);font-weight:bold;font-size:22px;font-family:calibri;"', '', $var);
        $var = str_replace('style="font-size:18px;color:rgb(27,90,169);font-weight:bold;font-family:calibri;"', '', $var);
        $var = str_replace('style="text-align:justify;line-height:0px;color:rgb(51,51,51);font-family:calibri;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"', '', $var);
        $var = str_replace('style="width: 1427.14px; display: inline-block; margin-top: 0px; margin-bottom: 30px; background: url(&quot;../images/CENACE-MARCADEAGUA.jpg&quot;) 0px 200px / 100% no-repeat scroll rgba(0, 0, 0, 0); min-height: 450px; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;', '', $var);
        $var = str_replace('style="display: inline-block; width: 713.562px; float: left; text-align: justify; padding: 10px;"', '', $var);
        $var = str_replace('style="style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="font-size: 18px; color: rgb(27, 90, 169); font-weight: bold; text-align: left;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px;"', '', $var);
        $var = str_replace('style="text-decoration: none; color: rgb(73, 169, 207);"', '', $var);
        $var = str_replace('style="width: 556.578px; display: inline-block; margin-bottom: 10px;"', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(&quot;../images/check.png&quot;); line-height: 18px;"', '', $var);
        $var = str_replace('style="font-size: 18px; color: rgb(27, 90, 169); font-weight: bold; font-family: calibri;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; font-family: calibri;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-family: calibri;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify;"', '', $var);
        $var = str_replace('style="padding-bottom: 10px; margin-bottom: 0px; color: rgb(73, 169, 207); font-size: 20px; font-weight: bold;"', '', $var);
        $var = str_replace('class="info-relevant"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207);"', '', $var);
        $var = str_replace('id="contenidos-right"', '', $var);
        $var = str_replace('id="contenidos-left"', '', $var);
        $var = str_replace('id="breadcrumb"', '', $var);
        $var = str_replace('id="breadcrumb"', '', $var);
        $var = str_replace('style="color: rgb(0, 0, 0);"', '', $var);
        $var = str_replace('style="background-color: transparent; border-bottom-color: rgb(194, 194, 194); border-bottom-style: solid; border-bottom-width: 1px; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 10px; text-align: center; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 1074.4px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri; font-size: 16px;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px;"', '', $var);
        $var = str_replace('style="font-size: 18px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri;"', '', $var);
        $var = str_replace('style="line-height: 0px; color: rgb(51, 51, 51); font-family: calibri;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left; font-family: calibri;"', '', $var);
        $var = str_replace('class="sub-vineta"', '', $var);
        $var = str_replace('style="line-height: 0px; text-align: justify;"', '', $var);
        $var = str_replace('style="background-color: rgb(255, 255, 255);"', '', $var);
        $var = str_replace('style="display: list-item; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: 497.2px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); display: block; float: none; font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; padding-bottom: 0px; padding-left: 40px; padding-right: 0px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 497.2px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-size: 16px; font-weight: 400;"', '', $var);
        $var = str_replace('bgcolor="#49A9CF"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-size: 16px; font-weight: 400;"', '', $var);
        $var = str_replace('style="line-height: 18px; list-style-image: url(http://cenace.upsa.edu.bo/images/check.png); margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify; width: 536px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); margin-bottom: 0px; text-align: justify;"', '', $var);
        $var = str_replace('align="center"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify; width: 536px;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri; padding-left: 50px;"', '', $var);
        $var = str_replace('style="margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="font-size:12.0pt;mso-bidi-font-family:Calibri"', '', $var);
        $var = str_replace('style="display: table-row-group; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;"', '', $var);
        $var = str_replace('style="line-height: 0px; list-style-image: url(http://cenace.upsa.edu.bo/images/check_round.jpg); margin-top: 20px; text-align: justify;"', '', $var);
        $var = str_replace('style="color: rgb(255, 255, 255); text-align: center;"', '', $var);
        $var = str_replace('style="line-height: 0px; margin-top: 20px; text-align: justify;"', '', $var);
        $var = str_replace('style="display: table-row; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;" bgcolor="#49a9cf"', '', $var);
        $var = str_replace('style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; letter-spacing: 0px;"', '', $var);
        $var = str_replace('style="text-align: justify;"', '', $var);
        $var = str_replace('bgcolor="#49a9cf"', '', $var);
        $var = str_replace('style="display: table-row-group; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;"><tr style="display: table-row; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 537.2px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 80px; padding-right: 80px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);

        // $var=strip_tags($var,'style=');

        // $var=str_replace('style','',$var);
        // $var = preg_replace('@style="|";"@i', '',$var);  
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 60px; padding-right: 60px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 60px; padding-right: 60px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(27, 90, 169); font-family: calibri; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(27, 90, 169); font-family: calibri; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); display: inline; float: none; font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: auto; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; margin-bottom: 0px; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="line-height: 18px; list-style-image: url(http://cenace.upsa.edu.bo/images/check.png); margin-bottom: 8px;"', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);

        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form>', '', $var);
        $var = str_replace('style="border-width: initial; border-style: none; width: 40px; margin-right: 3px; max-width: 100%;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri;"', '', $var);
        $var = str_replace('style="text-align: center; color: rgb(73, 169, 207); margin-bottom: 0px; font-weight: bold;"', '', $var);
        $var = str_replace('style="display: inline !important; float: none; background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);


        $var = str_replace('style="margin-bottom: 20px; text-align: center;"', '', $var);
        $var = str_replace('style="color: white; font-weight: bold; font-size: 20px; text-align: left; width: 445.203px; padding-top: 20px; padding-left: 17.7969px; font-family: calibri;"', '', $var);
        $var = str_replace('style="width: 445.203px; display: inline-block; margin-bottom: 10px;"', '', $var);
        $var = str_replace('style="background: rgb(1, 170, 209); color: white;"', '', $var);
        $var = str_replace('style="border: 3px solid rgb(75, 180, 176); border-radius: 7px; background: url(&quot;/images/fondoformulario.png&quot;) repeat;"', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url("../images/check.png"); line-height: 18px;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 570.766px; float: left; text-align: justify; padding: 10px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(73, 169, 207); font-family: calibri; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 445.203px; float: right;"', '', $var);
        $var = str_replace('style="margin-top: 20px; margin-bottom: 20px; padding-bottom: 20px; text-align: center; border-radius: 7px; vertical-align: top; background: url(&quot;../images/fondoformulario.png&quot;) repeat; max-width: 100%; width: 445.203px; border: 3px solid rgb(75, 180, 176);"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); text-align: center; border-bottom: 1px solid rgb(221, 221, 221); padding-top: 8px; padding-bottom: 8px;"', '', $var);
        $var = str_replace('<h2', '<b', $var);
        $var = str_replace('style="color: rgb(103, 105, 104); font-size: 14px; font-family: calibri; width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-width: 1px; border-style: solid; border-color: white;"', '', $var);
        $var = str_replace('</h2>', '</b>', $var);
        $var = str_replace('<h3', '<b', $var);
        $var = str_replace('</h3>', '</b>', $var);
        $var = str_replace('class="MsoNormal"', ' ', $var);
        $var = str_replace('margin-top: 0cm; margin-right: 20pt; margin-bottom: 0.0001pt; line-height: normal;', '', $var);
        $var = str_replace('margin-top: 0cm; margin-right: 20pt; margin-bottom: 0.0001pt; line-height: normal;', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(" ..="" images="" check.png");="" line-height:="" 18px;"=""', '', $var);
        $var = str_replace('style="display: block; height: 25px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: left;"', '', $var);
        $var = str_replace('style="display: block; height: 25px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: left;"', '', $var);
        $var = str_replace('style="color: rgb(204, 204, 204); float: left; list-style-image: none; list-style-position: outside; list-style-type: none; margin-bottom: 8px; padding-bottom: 0px; padding-left: 3px; padding-right: 3px; padding-top: 0px; vertical-align: middle;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 80px; padding-right: 0px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left; font-family: calibri; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-family: calibri; padding-left: 100px;"', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(" 18px;"="" check.png");="" images="" ..=""', '', $var);
        $var = str_replace('style="background-color: rgb(255, 255, 255); font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; letter-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; orphans: 2; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: center; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-attachment: scroll; background-clip: border-box; background-color: transparent; background-image: none; background-origin: padding-box; background-position-x: 0%; background-position-y: 0%; background-repeat: repeat; background-size: auto; float: left; margin-top: 20px; width: 53.72px;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-size: 25px; font-weight: 700; margin-bottom: 30px; text-align: center; text-transform: uppercase;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); display: block; float: left; font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; padding-top: 10px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 537.2px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-size: 22px; font-weight: 700; text-align: left;"', '', $var);
        $var = str_replace('style="letter-spacing: 0px; font-family: " helvetica="" neue",="" helvetica,="" arial,="" sans-serif;"=""', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-size: 22px; font-weight: 700; text-align: left;"', '', $var);
        $var = str_replace('style="font-family: calibri; text-align: center;"', '', $var);
        $var = str_replace('style="font-size: 24px !important; color: white !important;"', '', $var);
        $var = str_replace("’S", 's', $var);
        $var = str_replace("'s", 's', $var);

        $var = ereg_replace("<(/)?(font|span|del|ins|style)[^>]*>", "", $var);

        // $var = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\1>",$var);
        // $var = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\1>",$var);

        // $var = preg_replace('/[\t|\s{2,}]/', ' ', $var);  
        $var = eregi_replace("style=\"[^>]*\"", " ", $var);
        $var = eregi_replace("class=\"[^>]*\"", " ", $var);

        $var = strip_tags($var, "<br>,<b>,<p>,<strong>,<ul>,<li>,<div>,<img>,<a>,<iframe>,<table>,<tr>,<td>,<tbody>,<span>");


        return $var;
    }

    public function clearWord($var = null){
        $var = ereg_replace("<(/)?(font|del|ins|style)[^>]*>", "", $var);

        // $var = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\1>",$var);
        // $var = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\1>",$var);

        // $var = preg_replace('/[\t|\s{2,}]/', ' ', $var);  
        //$var = eregi_replace("style=\"[^>]*\"", " ", $var);
        $var = eregi_replace("class=\"[^>]*\"", " ", $var);

        $var = strip_tags($var, "<br>,<b>,<p>,<strong>,<ul>,<li>,<div>,<img>,<a>,<iframe>,<table>,<tr>,<td>,<tbody>,<span>");


        return $var;
    }

    public function limpiarCadenaWord($var)
    {

        $var = str_replace('style="margin-bottom:8px;list-style-image:url(&quot;../images/check.png&quot;);line-height:18px;"', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(&quot;../images/check_round.jpg&quot;); line-height: 18px;"', '', $var);
        $var = str_replace('style="color:rgb(73,169,207);"', '', $var);
        $var = str_replace('class="MsoNormal"', '', $var);
        $var = str_replace('<o:p></o:p>', '', $var);
        $var = str_replace('·&nbsp; &nbsp;', '', $var);
        $var = str_replace('<p <', '<', $var);
        $var = str_replace('<b <', '<', $var);
        $var = str_replace("·&nbsp;", "", $var);
        $var = str_replace("&nbsp;", "", $var);
        $var = str_replace('<strong <', '<', $var);
        $var = str_replace('<ul <', '<', $var);
        $var = str_replace('<li <', '<', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"', '', $var);
        $var = str_replace('style="margin: 1.2pt 25.25pt 0.0001pt 0.6pt;"', '', $var);
        $var = str_replace(`style="margin-top:.55pt;margin-right:194.45pt;margin-bottom:0cm;margin-left:.6pt;margin-bottom:.0001pt;line-height:23.0pt;mso-line-height-rule:exactly"`, '', $var);
        $var = str_replace('style="" calibri","sans-serif";mso-fareast-font-family:="" calibri;color:#006fc0;mso-ansi-language:es-bo"=""', '', $var);
        $var = str_replace('style="margin-top:.55pt;margin-right:194.45pt;margin-bottom:0cm;margin-left:.6pt;margin-bottom:.0001pt;line-height:23.0pt;mso-line-height-rule:exactly"', '', $var);
        $var = str_replace('style="margin-bottom: 7px; color: rgb(52, 51, 60); text-align: justify;"', '', $var);
        $var = str_replace('style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal"', '', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="margin-top:12.0pt;margin-right:0cm;margin-bottom:12.0pt;margin-left:0cm;line-height:normal"', '', $var);
        $var = str_replace('style="margin-bottom: 0.0001pt; line-height: normal;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 418.672px; float: right;"', '', $var);
        $var = str_replace('style="width: 418.672px; display: inline-block; margin-bottom: 10px;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 536.766px; float: left; text-align: justify; padding: 10px;"', '', $var);
        $var = str_replace('style="line-height: 0px;"', '', $var);
        $var = str_replace('style="color: white; text-align: center;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify; width: 714px;"', '', $var);
        $var = str_replace('style="line-height: 0px; margin-top: 20px;"', '', $var);
        $var = str_replace('style="text-align:justify;color:rgb(51,51,51);margin-bottom:0px;font-family:calibri;"', '', $var);
        $var = str_replace('style="color:rgb(73,169,207);font-weight:bold;font-size:22px;font-family:calibri;"', '', $var);
        $var = str_replace('style="font-size:18px;color:rgb(27,90,169);font-weight:bold;font-family:calibri;"', '', $var);
        $var = str_replace('style="text-align:justify;line-height:0px;color:rgb(51,51,51);font-family:calibri;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"', '', $var);
        $var = str_replace('style="width: 1427.14px; display: inline-block; margin-top: 0px; margin-bottom: 30px; background: url(&quot;../images/CENACE-MARCADEAGUA.jpg&quot;) 0px 200px / 100% no-repeat scroll rgba(0, 0, 0, 0); min-height: 450px; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;', '', $var);
        $var = str_replace('style="display: inline-block; width: 713.562px; float: left; text-align: justify; padding: 10px;"', '', $var);
        $var = str_replace('style="style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="font-size: 18px; color: rgb(27, 90, 169); font-weight: bold; text-align: left;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px;"', '', $var);
        $var = str_replace('style="text-decoration: none; color: rgb(73, 169, 207);"', '', $var);
        $var = str_replace('style="width: 556.578px; display: inline-block; margin-bottom: 10px;"', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(&quot;../images/check.png&quot;); line-height: 18px;"', '', $var);
        $var = str_replace('style="font-size: 18px; color: rgb(27, 90, 169); font-weight: bold; font-family: calibri;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; font-family: calibri;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-family: calibri;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify;"', '', $var);
        $var = str_replace('style="padding-bottom: 10px; margin-bottom: 0px; color: rgb(73, 169, 207); font-size: 20px; font-weight: bold;"', '', $var);
        $var = str_replace('class="info-relevant"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207);"', '', $var);
        $var = str_replace('id="contenidos-right"', '', $var);
        $var = str_replace('id="contenidos-left"', '', $var);
        $var = str_replace('id="breadcrumb"', '', $var);
        $var = str_replace('id="breadcrumb"', '', $var);
        $var = str_replace('style="color: rgb(0, 0, 0);"', '', $var);
        $var = str_replace('style="background-color: transparent; border-bottom-color: rgb(194, 194, 194); border-bottom-style: solid; border-bottom-width: 1px; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 10px; text-align: center; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 1074.4px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri; font-size: 16px;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px;"', '', $var);
        $var = str_replace('style="font-size: 18px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri;"', '', $var);
        $var = str_replace('style="line-height: 0px; color: rgb(51, 51, 51); font-family: calibri;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left; font-family: calibri;"', '', $var);
        $var = str_replace('class="sub-vineta"', '', $var);
        $var = str_replace('style="line-height: 0px; text-align: justify;"', '', $var);
        $var = str_replace('style="background-color: rgb(255, 255, 255);"', '', $var);
        $var = str_replace('style="display: list-item; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: 497.2px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); display: block; float: none; font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; padding-bottom: 0px; padding-left: 40px; padding-right: 0px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 497.2px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-size: 16px; font-weight: 400;"', '', $var);
        $var = str_replace('bgcolor="#49A9CF"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-size: 16px; font-weight: 400;"', '', $var);
        $var = str_replace('style="line-height: 18px; list-style-image: url(http://cenace.upsa.edu.bo/images/check.png); margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify; width: 536px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); margin-bottom: 0px; text-align: justify;"', '', $var);
        $var = str_replace('align="center"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri; text-align: justify; width: 536px;"', '', $var);
        $var = str_replace('style="text-align: justify; color: rgb(51, 51, 51); margin-bottom: 0px; font-family: calibri; padding-left: 50px;"', '', $var);
        $var = str_replace('style="margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="font-size:12.0pt;mso-bidi-font-family:Calibri"', '', $var);
        $var = str_replace('style="display: table-row-group; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;"', '', $var);
        $var = str_replace('style="line-height: 0px; list-style-image: url(http://cenace.upsa.edu.bo/images/check_round.jpg); margin-top: 20px; text-align: justify;"', '', $var);
        $var = str_replace('style="color: rgb(255, 255, 255); text-align: center;"', '', $var);
        $var = str_replace('style="line-height: 0px; margin-top: 20px; text-align: justify;"', '', $var);
        $var = str_replace('style="display: table-row; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;" bgcolor="#49a9cf"', '', $var);
        $var = str_replace('style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; letter-spacing: 0px;"', '', $var);
        $var = str_replace('style="text-align: justify;"', '', $var);
        $var = str_replace('bgcolor="#49a9cf"', '', $var);
        $var = str_replace('style="display: table-row-group; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;"><tr style="display: table-row; float: none; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; width: auto;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 537.2px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 80px; padding-right: 80px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);

        // $var=strip_tags($var,'style=');

        // $var=str_replace('style','',$var);
        // $var = preg_replace('@style="|";"@i', '',$var);  
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 60px; padding-right: 60px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 60px; padding-right: 60px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(27, 90, 169); font-family: calibri; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(27, 90, 169); font-family: calibri; font-size: 18px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); display: inline; float: none; font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: auto; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; margin-bottom: 0px; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="line-height: 18px; list-style-image: url(http://cenace.upsa.edu.bo/images/check.png); margin-bottom: 8px;"', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);

        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('<form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form>', '', $var);
        $var = str_replace('style="border-width: initial; border-style: none; width: 40px; margin-right: 3px; max-width: 100%;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); font-family: calibri;"', '', $var);
        $var = str_replace('style="text-align: center; color: rgb(73, 169, 207); margin-bottom: 0px; font-weight: bold;"', '', $var);
        $var = str_replace('style="display: inline !important; float: none; background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);


        $var = str_replace('style="margin-bottom: 20px; text-align: center;"', '', $var);
        $var = str_replace('style="color: white; font-weight: bold; font-size: 20px; text-align: left; width: 445.203px; padding-top: 20px; padding-left: 17.7969px; font-family: calibri;"', '', $var);
        $var = str_replace('style="width: 445.203px; display: inline-block; margin-bottom: 10px;"', '', $var);
        $var = str_replace('style="background: rgb(1, 170, 209); color: white;"', '', $var);
        $var = str_replace('style="border: 3px solid rgb(75, 180, 176); border-radius: 7px; background: url(&quot;/images/fondoformulario.png&quot;) repeat;"', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url("../images/check.png"); line-height: 18px;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 570.766px; float: left; text-align: justify; padding: 10px;"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); margin-bottom: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(73, 169, 207); font-family: calibri; font-size: 22px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="display: inline-block; width: 445.203px; float: right;"', '', $var);
        $var = str_replace('style="margin-top: 20px; margin-bottom: 20px; padding-bottom: 20px; text-align: center; border-radius: 7px; vertical-align: top; background: url(&quot;../images/fondoformulario.png&quot;) repeat; max-width: 100%; width: 445.203px; border: 3px solid rgb(75, 180, 176);"', '', $var);
        $var = str_replace('style="color: rgb(51, 51, 51); text-align: center; border-bottom: 1px solid rgb(221, 221, 221); padding-top: 8px; padding-bottom: 8px;"', '', $var);
        $var = str_replace('<h2', '<b', $var);
        $var = str_replace('style="color: rgb(103, 105, 104); font-size: 14px; font-family: calibri; width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-width: 1px; border-style: solid; border-color: white;"', '', $var);
        $var = str_replace('</h2>', '</b>', $var);
        $var = str_replace('<h3', '<b', $var);
        $var = str_replace('</h3>', '</b>', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(" ..="" images="" check.png");="" line-height:="" 18px;"=""', '', $var);
        $var = str_replace('style="display: block; height: 25px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: left;"', '', $var);
        $var = str_replace('style="display: block; height: 25px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: left;"', '', $var);
        $var = str_replace('style="color: rgb(204, 204, 204); float: left; list-style-image: none; list-style-position: outside; list-style-type: none; margin-bottom: 8px; padding-bottom: 0px; padding-left: 3px; padding-right: 3px; padding-top: 0px; vertical-align: middle;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; line-height: 0px; orphans: 2; padding-bottom: 0px; padding-left: 80px; padding-right: 0px; padding-top: 0px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-weight: bold; font-size: 22px; text-align: left; font-family: calibri; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;"', '', $var);
        $var = str_replace('style="text-align: justify; line-height: 0px; color: rgb(51, 51, 51); font-family: calibri; padding-left: 100px;"', '', $var);
        $var = str_replace('<div  ><div ><div class="sidebar-whatsapp" ><a href="https://api.whatsapp.com/send?phone=59173656072" target="_blank" style="font-size: 24px !important; color: white !important;"><b >COMUNICATE CON NOSOTROS</b><div ><img src="http://cenace.upsa.edu.bo/images/whatsapp-logo.png" alt="" >&nbsp;73656072</div></a></div></div><p >Si desea más información puede apersonarse por la Av. Paraguá y 4to. Anillo, llamar al teléfono 3464000 int 218, al nro. de Celular 736-56072, al email&nbsp;<a href="mailto:cenace@upsa.edu.bo" target="_blank">cenace@upsa.edu.bo</a>&nbsp;o escribir su consulta en el formulario.</p><div id="form-contact-page" ><form name="formu" id="formu" method="post" action="http://cenace.upsa.edu.bo/Contacto/Enviar-Mensaje/"><b >SOLICITA INFORMACIÓN</b><div ><input type="text" id="text-name" name="nombre" placeholder="Nombres y Apellidos(*)" ></div><div ><input type="text" id="text-phone" name="telefono" placeholder="Teléfono(*)" ></div><div ><input type="text" id="text-email" name="email" placeholder="Correo electrónico(*)" ></div><div ><input type="text" id="programa" name="taller" value="Programa de inglés" placeholder="Curso ó Taller de interés" ></div><div ><textarea rows="10" id="text-message" name="mensaje" placeholder="Mensaje" style="width: 378.422px; margin-top: 5px; padding: 7px; border-radius: 5px; border-color: white; font-family: calibri; font-size: 14px; color: rgb(103, 105, 104);"></textarea></div><div style="font-family: calibri; color: white; margin-top: 10px; margin-left: 26.7031px; text-align: left;">Antes de enviar su información realizar clic en la caja de verificación de abajo</div><div class="g-recaptcha" data-sitekey="6Lcb7CYUAAAAAA7LfGbLMclNg-DPANWk_kdK3s82" style="color: rgb(51, 51, 51); font-family: calibri; display: inline-block; margin-top: 1em; margin-bottom: 1em;"><div style="width: 304px; height: 78px;"></div></div></form></div></div>', '', $var);
        $var = str_replace('style="margin-bottom: 8px; list-style-image: url(" 18px;"="" check.png");="" images="" ..=""', '', $var);
        $var = str_replace('style="background-color: rgb(255, 255, 255); font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; letter-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; orphans: 2; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: center; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="background-attachment: scroll; background-clip: border-box; background-color: transparent; background-image: none; background-origin: padding-box; background-position-x: 0%; background-position-y: 0%; background-repeat: repeat; background-size: auto; float: left; margin-top: 20px; width: 53.72px;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-size: 25px; font-weight: 700; margin-bottom: 30px; text-align: center; text-transform: uppercase;"', '', $var);
        $var = str_replace('style="background-color: transparent; color: rgb(51, 51, 51); display: block; float: left; font-family: calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; padding-top: 10px; text-align: justify; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; width: 537.2px; word-spacing: 0px;"', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-size: 22px; font-weight: 700; text-align: left;"', '', $var);
        $var = str_replace('style="letter-spacing: 0px; font-family: " helvetica="" neue",="" helvetica,="" arial,="" sans-serif;"=""', '', $var);
        $var = str_replace('style="color: rgb(73, 169, 207); font-size: 22px; font-weight: 700; text-align: left;"', '', $var);
        $var = str_replace('style="font-family: calibri; text-align: center;"', '', $var);
        $var = str_replace('style="font-size: 24px !important; color: white !important;"', '', $var);
        $var = str_replace("’S", 's', $var);
        $var = str_replace("'s", 's', $var);

        $var = ereg_replace("<(/)?(font|span|del|ins|style)[^>]*>", "", $var);

        // $var = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\1>",$var);
        // $var = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\1>",$var);

        // $var = preg_replace('/[\t|\s{2,}]/', ' ', $var);  
        $var = eregi_replace("style=\"[^>]*\"", " ", $var);
        $var = eregi_replace("class=\"[^>]*\"", " ", $var);

        $var = strip_tags($var, "<b>,<p>,<strong>,<ul>,<li>,<ol>,<div>,<img>,<a>,<iframe>,<table>.<tr>,<tb>,<br>");




        return $var;
    }

    public function remplazoNucleo($var)
    {
        $var = str_replace('{_RUTA_WEB}', _RUTA_WEB, $var);
        $var = str_replace('{_RUTA_WEB_NUCLEO}', _RUTA_WEB_NUCLEO, $var);
        $var = str_replace('{_RUTA_IMAGES}', _RUTA_IMAGES, $var);
        $var = str_replace('{_RUTA_DOCS}', _RUTA_DOCS, $var);
        $var = str_replace('{icon:phone}', '<i class="icn icn-phone"></i>', $var);
        $var = str_replace('{icon:email}', '<i class="icn icn-email"></i>', $var);
        $var = str_replace('{icon:whatsapp}', '<i class="icn icn-whatsapp"></i>', $var);
        $var = str_replace('{icon:facebook}', '<i class="icn icn-facebook"></i>', $var);
        $var = str_replace('{icon:twitter}', '<i class="icn icn-twitter"></i>', $var);
        $var = str_replace('{icon:linkedin}', '<i class="icn icn-linkedin"></i>', $var);
        $var = str_replace('{icon:youtube}', '<i class="icn icn-youtube"></i>', $var);
        $var = str_replace('{icon:instagram}', '<i class="icn icn-instagram"></i>', $var);

        return $var;
    }
}