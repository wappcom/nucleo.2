<?PHP
header("Content-Type: text/html;charset=utf-8");

class CONFIG{

	var $fmt;

    function __construct($fmt) {
        $this->fmt = $fmt;
    }

    public function versionConf(){
		$consulta = "SELECT conf_version FROM configuracion";
		$rs =$this->fmt->query->consulta($consulta);
		$row=$this->fmt->query->obtFila($rs);
		return  $row["conf_version"];
		$this->fmt->query->liberarConsulta();
    	return "";
    }

    public function versionPlus(){
    	$consulta = "SELECT conf_version FROM configuracion";
		$rs =$this->fmt->query->consulta($consulta);
		$row=$this->fmt->query->obtFila($rs);

		$vs = $row["conf_version"] + 1;
		$sql="UPDATE configuracion SET conf_version='".$vs."'";
		$this->fmt->query->consulta($sql);

		return  $vs;
	}

	public function datos()
	{
		$consulta = "SELECT * FROM configuracion";
		$rs = $this->fmt->query->consulta($consulta);
		return $this->fmt->query->obtFila($rs);
	}
}