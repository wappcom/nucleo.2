<?php
header('Content-Type: text/html; charset=utf-8');

class ARCHIVO{

	var $fmt;

	function __construct($fmt) {
		$this->fmt = $fmt;
	}

	public function urlAdd($url,$add){
		$ruta = explode("/", $url);
		$nombre = end($ruta);
		$ruta_x = str_replace($nombre,'',$url);
		$nombrex = $this->archivoNombre($nombre);
		$extencion = $this->archivoExtension($url);
		return  $ruta_x.$nombrex.$add.".".$extencion;
  	}

  	public function archivoNombre($archivo){
    	$trozos = pathinfo($archivo);
    	return $trozos["filename"];
  	}

  	public function archivoExtension($archivo){
    	$trozos = pathinfo($archivo);
    	return $trozos["extension"];
  	}

  	function eliminarArchivoUrl($url){
    	unlink($url);
  	}

    public function docLinkIds($vars){
        $arrayIds = explode(",", $vars["ids"]);
        $cont = count ($arrayIds);
        $aux ="";

        for ($i=0; $i < $cont; $i++) { 
          if ($i == $cont -1){
            $aux .= ' doc_id IN (SELECT doc_cat_doc_id FROM documento_categorias WHERE doc_cat_cat_id = "'.$arrayIds[$i].'" )';
          }else{
            $aux .=  ' doc_id IN (SELECT doc_cat_doc_id FROM documento_categorias WHERE doc_cat_cat_id = "'.$arrayIds[$i].'" ) AND';
          }
        }

        $sql = "SELECT doc_url FROM documento WHERE ".$aux." AND doc_activar='1' ORDER BY doc_fecha DESC Limit 1";
        $rss= $this->fmt->query->consulta($sql);
        $row= $this->fmt->query->obt_fila($rss);   

        return $row["doc_url"];
        //return $sql;
    }
}