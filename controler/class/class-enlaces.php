<?PHP
header("Content-Type: text/html;charset=utf-8");

class ENLACE{

	var $fmt;

    function __construct($fmt) {
        $this->fmt = $fmt;
    }

    public function datosId($id){
      	$consulta = "SELECT * FROM  enlace WHERE enl_id='$id'";
      	$rs =$this->fmt->query->consulta($consulta,__METHOD__);
      	$num=$this->fmt->query->num_registros($rs);
      		if($num>0){
      			$row=$this->fmt->query->obt_fila($rs);
      			return $row;
      		}else{
      			return 0;
      		}
      	$this->fmt->query->liberar_consulta();
    }

    public function categoriaIdArray($cat,$activar='1'){
        $consulta="SELECT DISTINCT enl_id,enl_nombre,enl_descripcion,enl_link,enl_target,enl_imagen,enl_clases FROM enlace, enlace_categorias  WHERE enl_cat_cat_id='".$cat."' AND enl_cat_enl_id=enl_id AND enl_activar='".$activar."'";
        $rs =$this->fmt->query->consulta($consulta,__METHOD__);
        $num = $this->fmt->query->numRegistros($rs);
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->query->obtFila($rs);
                $enl["enl_id"] = $row["enl_id"];
                $enl["enl_nombre"] = $row["enl_nombre"];
                $enl["enl_descripcion"] = $row["enl_descripcion"];
                $enl["enl_link"] = $row["enl_link"];
                $enl["enl_target"] = $row["enl_target"];
                $enl["enl_imagen"] = $row["enl_imagen"];
                $enl["enl_clases"] = $row["enl_clases"];
            }
            return $enl;
        }else{
            return 0;
        }
        $this->fmt->query->liberarConsulta($rs);
    }

    public function categoriaIdLista($vars){
        $cat = $vars["cat"];
        $activar = $this->fmt->emptyReturn($vars["activar"],"1");
        $modo = $this->fmt->emptyReturn($vars["modo"],"default");

        $consulta="SELECT DISTINCT enl_id,enl_nombre,enl_descripcion,enl_link,enl_target,enl_imagen,enl_clases FROM enlace, enlace_categorias  WHERE enl_cat_cat_id='".$cat."' AND enl_cat_enl_id=enl_id AND enl_activar='".$activar."'";
        $rs =$this->fmt->query->consulta($consulta,__METHOD__);
        $num = $this->fmt->query->numRegistros($rs);
        $html = "";
        if($num>0){
            for($i=0;$i<$num;$i++){
                $row=$this->fmt->query->obtFila($rs);
                $enl_id = $row["enl_id"];
                $enl_nombre = $row["enl_nombre"];
                $enl_descripcion = $row["enl_descripcion"];
                $enl_link = str_replace("{_RUTA_WEB}",_RUTA_WEB,$row["enl_link"]);
                $enl_target = $this->fmt->emptyReturn($row["enl_target"],"_self");
                $enl_imagen = $row["enl_imagen"];
                $enl_class = $row["enl_clases"];

                $html .= '<li>';
                $html .= '<a target="'.$enl_target.'" class="link link-'.$i.' '.$enl_class.'" href="'.$enl_link.'" title="'.$enl_nombre.'" item="'.$enl_id.'">';

                if ($modo=="default"){    
                    $html .= '<span class="nombre">'.$enl_nombre.'</span>';
                }                 

                if ($modo=="imagen"){
                    $html .= '<img class="imagen" src="'.$enl_imagen.'">';
                }                  

                if ($modo=="imagen-nombre"){
                    $html .= '<img class="imagen" src="'.$enl_imagen.'">';
                    $html .= '<span class="nombre">'.$enl_nombre.'</span>';
                }                   

                if ($modo=="nombre-descripcion-imagen"){   
                    $html .= '<span class="nombre">'.$enl_nombre.'</span>';
                    $html .= '<span class="descripcion">'.$enl_nombre.'</span>';
                    $html .= '<img class="imagen" src="'.$enl_imagen.'">';
                }                 

                if ($modo=="imagen-background"){
                    $html .= '<div class="bg" style="background:url('.$enl_imagen.') no-repeat center center">';
                }                 

                if ($modo=="nombre-imagen-background"){
                    $html .= '<span class="nombre">'.$enl_nombre.'</span>';
                    $html .= '<div class="bg" style="background:url('.$enl_imagen.') no-repeat center center">';
                }                

                if ($modo=="nombre-descripcion-imagen-background"){
                    $html .= '<span class="nombre">'.$enl_nombre.'</span>';
                    $html .= '<span class="descripcion">'.$enl_nombre.'</span>';
                    $html .= '<div class="bg" style="background:url('.$enl_imagen.') no-repeat center center">';
                }   

                $html .= '</a>';
                $html .= '</li>';
            }
            return $html;
        }else{
            return 0;
        }
        $this->fmt->query->liberarConsulta($rs);
    }

}
