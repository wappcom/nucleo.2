<?php
header('Content-Type: text/html; charset=utf-8');

class CONSTRUCTOR
{

    var $archivo;
    var $archivos;
    var $atajo;
    var $autentificacion;
    var $brand;
    var $categoria;
    var $class_documento;
    var $class_modulo;
    var $class_multimedia;
    var $class_pagina;
    var $class_sistema;
    var $class_sitios;
    var $config;
    var $contenido;
    var $cookies;
    var $enlace;
    var $errores;
    var $finder;
    var $footer;
    var $form;
    var $get;
    var $header;
    var $lang;
    var $mail;
    var $media;
    var $mensaje;
    var $modulo;
    var $nav;
    var $page;
    var $plantilla;
    var $publicacion;
    var $query;
    var $redireccion;
    var $sesion;
    var $sistema;
    var $sitio;
    var $usuario;
    var $validar;
    var $valores;
    var $vs;


    function __construct()
    {

        header("Content-Type: text/html;charset=utf-8");

        array_walk($_POST, 'limpiar_cadena');
        array_walk($_GET, 'limpiar_cadena');

        if (isset($_GET["mod_id"])) {
            if (!is_numeric($_GET["mod_id"])) {
                define('_ID_MODULO', $_GET["mod_id"]);
            }
        }

        if (isset($_GET["query"])) {
            echo "error";
            exit(0);
        }

        if (isset($_GET["id"])) {
            if (!is_numeric($_GET["id"])) {
                header("Location:../index.php?s=" . _RUTA_DEFAULT . "&cat=" . $cat . "&pla=" . $pla . "&tarea=error-id");
            }
        }



        require_once(_RUTA_NUCLEO . "controler/class/class-sesion.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-mysql.php");

        $sesion = new SESION($this);
        $sesion->iniciar_sesion();
        $this->sesion = $sesion;

        $query = new MYSQL();
        $query->conectar(_BASE_DE_DATOS, _HOST, _USUARIO, _PASSWORD);
        $this->query = $query;

        require_once(_RUTA_NUCLEO . "controler/class/class-get.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-lang.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-plantilla.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-errores.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-redireccion.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-mensajes.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-validar.php");

        require_once(_RUTA_NUCLEO . "controler/class/class-autentificacion.php");

        require_once(_RUTA_NUCLEO . "controler/class/class-page.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-brand.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-usuario.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-header.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-footer.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-sistemas.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-sistema.php");

        require_once(_RUTA_NUCLEO . "controler/class/class-modulos.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-modulo.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-paginas.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-nav.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-categoria.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-mail.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-archivos.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-archivo.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-form.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-publicacion.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-sitios.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-sitio.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-multimedia.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-documentos.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-finder.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-valores.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-cookies.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-enlaces.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-contenido.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-config.php");
        require_once(_RUTA_NUCLEO . "controler/class/class-media.php");
        require_once(_RUTA_NUCLEO . "modulos/nav/atajo.class.php");



        $this->get = new GET($this);
        $this->validar = new VALIDAR($this);
        $this->autentificacion = new AUTENTIFICACION($this);
        $this->plantilla = new PLANTILLA($this);

        $this->brand = new BRAND($this);
        $this->mensaje = new MENSAJE($this);
        $this->errores = new ERRORES($this);
        $this->usuario = new USUARIO($this);
        $this->redireccion =  new REDIRECCION($this);
        $this->header = new CLASSHEADER($this);
        $this->footer = new CLASSFOOTER($this);
        $this->class_pagina = new CLASSPAGINAS($this);
        $this->class_modulo = new CLASSMODULOS($this);
        $this->modulo = new MODULO($this);
        $this->class_sistema = new CLASSSISTEMAS($this);
        $this->sistema = new SISTEMA($this);
        $this->class_multimedia = new CLASSMULTIMEDIA($this);
        $this->class_documento = new CLASSDOCUMENTO($this);
        $this->nav = new NAV($this);
        $this->categoria = new CATEGORIA($this);
        $this->mail = new MAIL($this);
        $this->archivo = new ARCHIVO($this);
        $this->archivos = new ARCHIVOS($this);
        $this->form = new FORM($this);
        $this->publicacion = new PUBLICACION($this);
        $this->class_sitios = new CLASSSITIOS($this);
        $this->finder = new FINDER($this);
        $this->valores = new VALORES($this);
        $this->cookies = new COOKIES($this);
        $this->enlace = new ENLACE($this);
        $this->contenido = new CONTENIDO($this);
        $this->lang = new LANG($this);
        $this->page = new PAGE($this);
        $this->sitio = new SITIO($this);
        $this->atajo = new ATAJO($this);
        $this->media = new MEDIA($this);
        $this->config = new CONFIG($this);
    }

    function limpiar_cadena($valor)
    {
        $valor = str_ireplace("SELECT", "", $valor);
        $valor = str_ireplace("COPY", "", $valor);
        $valor = str_ireplace("DELETE", "", $valor);
        $valor = str_ireplace("query", "", $valor);
        $valor = str_ireplace("QUERY", "", $valor);
        $valor = str_ireplace("DROP", "", $valor);
        $valor = str_ireplace("DUMP", "", $valor);
        $valor = str_ireplace(" OR ", "", $valor);
        $valor = str_ireplace(" AND ", "", $valor);
        $valor = str_ireplace("AND ", "", $valor);
        $valor = str_ireplace("%", "", $valor);
        $valor = str_ireplace("LIKE", "", $valor);
        $valor = str_ireplace("--", "", $valor);
        $valor = str_ireplace("^", "", $valor);
        $valor = str_ireplace("[", "", $valor);
        $valor = str_ireplace("]", "", $valor);
        $valor = str_ireplace("\'", "", $valor);
        $valor = str_ireplace("!", "", $valor);
        $valor = str_ireplace("¡", "", $valor);
        $valor = str_ireplace("?", "", $valor);
        $valor = str_ireplace("=", "", $valor);
        $valor = str_ireplace("&", "", $valor);
        //$valor = addslashes($valor);
        return $valor;
    }

    function consoleLog($valor)
    {
        return "<script type='text/javascript'>console.log('".$valor."')</script>";
    }

    public function randNum()
    {
        $rand = range(1000, 10000);
        shuffle($rand);
        foreach ($rand as $val) {
            $vrand = $val;
        }
        return $vrand;
    }

    function emptyReturn($valor, $retorno)
    {
        if (empty($valor)) {
            return $retorno;
        } else {
            return $valor;
        }
    }

    function remplazarRuta($str)
    {
        $retorno = str_replace('{_RUTA_WEB}', _RUTA_WEB, $str);
        $retorno = str_replace('{{_PATH_WEB}}', _RUTA_WEB, $str);
        $retorno = str_replace('{_RUTA_SERVER}', _RUTA_SERVER, $retorno);
        $retorno = str_replace('{{_PATH_SERVER}}', _RUTA_SERVER, $retorno);
        $retorno = str_replace('{_RUTA_HOST}', _RUTA_HOST, $retorno);
        $retorno = str_replace('{{_PATH_HOST}}', _RUTA_HOST, $retorno);
        return $retorno;
    }
}
