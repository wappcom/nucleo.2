<?PHP
header("Content-Type: text/html;charset=utf-8");

class PAGE{

	var $fmt;

	function __construct($fmt) {
		$this->fmt = $fmt;
	}

	//Pagina

  	public function headerPageMod($vars){
  		$idMod = $vars["idMod"];
  		$id_mod = $vars["id_mod"];
  		$botones = $this->fmt->emptyReturn($vars["botones"],"");
  		$modo = $this->fmt->emptyReturn($vars["modo"],"estandar");
  		$class = $vars["classCss"];
  		$classMod = $vars["classMod"];
  		$css = $vars["css"];
  		$title = $vars["title"];
  		$icon = $vars["icon"];
  		$html = "";

		if(!empty($css)){
			$html .= '<link rel="stylesheet" href="'.$css.'?'.$this->fmt->randNum().'" type="text/css" media="screen" />';
		}	

		if((!empty($idMod)) || (!empty($id_mod)) ){
			if (!empty($idMod)){
				$modArray = $this->fmt->modulo->datosId($idMod);
			}
			if (!empty($id_mod)){
				$modArray = $this->fmt->modulo->datosId($id_mod);
			}
			$nom = $modArray["mod_nombre"]; 
			$rutaAmigable = $modArray["mod_ruta_amigable"]; 
			$color = $modArray["mod_color"]; 
			$icon = $modArray["mod_icono"]; 
			$this->titlePage(array('title' => $nom));
		}else{
			$nom = $title;
			$this->titlePage(array('title' => $nom));
		}

		switch ($modo) {
			case 'estandar':
				$html .= '<div class="head-modulo head-modulo-'.$rutaAmigable.'">';
				$html .= '	<div class="container">';
				$html .= '		<h1 class="title pull-left">';
				$html .= '			<i class="'.$icon.'" style="color:'.$color.'"></i>'.$nom; 
				$html .= '		</h1>';
				if (!empty($botones)){
					$html .= ' <div class="head-botones pull-right">';
					$html .= $botones;
					$html .= ' </div>';
				}  
				$html .= '	</div>';
				$html .= '</div>';
			break;

			case 'estandar-mod':
				$html .= '<div class="head-modulo head-modulo-'.$rutaAmigable.'">';
				$html .= '	<div class="container">';
				$html .= '		<h1 class="title pull-left">';
				$html .= '			<i class="'.$icon.'" style="color:'.$color.'"></i>'.$nom; 
				$html .= '		</h1>';

				if (!empty($botones)){
					$html .= '<div class="head-botones pull-right">';
					$html .= $botones;
					$html .= '</div>';
				}  
				$html .= '	</div>';
				$html .= '</div>';
				$html .= '<div class="body-modulo container-fluid body-modulo-'.$rutaAmigable.' '.$classMod.'">';
				$html .= '	<div class="container">';
			break;

			case 'sidebar-mod':
				$html .= '<div class="sidebar">';
				$html .= '	<h1 class="title">';
				$html .= '		<i class="'.$icon.'" style="color:'.$color.'"></i><span>'.$nom.'</span>'; 
				$html .= '	</h1>';
				if (!empty($botones)){
				$html .= '	<div class="botones">';
				$html .= $botones;
				$html .= '	</div>';
				}
				$html .= '</div>';
			break;

		}
		return $html;
	}  

	public function headModuloInner($vars){

		$class = $vars["class"];
		$btnDefault = $vars["btnDefault"];
  		$botones =  $this->fmt->emptyReturn($vars["botones"],"");
  		$title = $vars["title"];
  		$idMod = $vars["idMod"];
  		$idRender = $vars["idRender"];

		switch ($btnDefault) {
			case 'crear':
				//$btn =$this->fmt->class_pagina->crear_btn_m("Crear","icn-plus","","btn btn-primary btn-menu-ajax btn-new btn-small",$idMod,"form_nuevo");
				$btn = $this->btnFormNuevo(array('idMod' => $idMod,'nombre'=> 'Crear','idRender'=> $idRender ));
				break;
			
			default:
				$btn="";
				break;
		}

		$html .= '';
		$html .= '<div class="container-fluid head-modulo-inner headModuloInner '.$class.'">';
		$html .= '	<div class="title">'.$title.'</div>';
		$html .= '	<div class="buttons">';
		//$html = '		<a class="btn btn-primary btn-menu-ajax btn-new btn-small" title="Crear" id="btn-m-2" id_mod="2" nombre="" vars="form_nuevo">';
		//$html = '	 	<i class="icn-plus"></i><span> Crear </span></a> </div>';
		$html .= $botones.$btn ;
		$html .= '	</div>';
		$html .= '</div>';

		return $html;
	}

	public function footerPageMod($vars){
  		$id_mod = $vars["idMod"];
  		$modo = $this->fmt->emptyReturn($vars["modo"],"estandar");
  		$class = $vars["classCss"];
  		$classMod = $vars["classMod"];
  		$html = "";


		switch ($modo) {
			case 'estandar':
				$html .= '	</div><!-- fin container mod -->';
				$html .= '</div><!-- fin container-fluid mod -->';
			break;
		}
		return $html;
	}
	
	public function titlePage($vars){
		$title = $vars["title"];
		$cat = $this->fmt->emptyReturn($vars["cat"],0);

		if ($title!=""){
			$pageTitle = $title." - ".$this->fmt->sitio->nombreSitio(array('cat' => $cat ));
		}else{
			$pageTitle = $this->fmt->sitio->nombreSitio(array('cat' => $cat ));
		}
		$html  ='';
    	$html .= '<script type="text/javascript">';
    	$html .= '	$(document).ready(function() {';
    	$html .= '		$("title").html("'.$pageTitle.'");';
    	$html .= '	});';
    	$html .= '</script>';

    	return $html;
  	}

	public function modHead($vars){
		
  		$title = $vars["title"];
  		$class = $vars["class"];
  		$modo = $vars["modo"];

  		$btnLeft = $this->fmt->emptyReturn($vars["btnLeft"],"");
  		$btnLight = $this->fmt->emptyReturn($vars["btnLight"],"");

  		$html  = '';
  		$html .= '<div class="pageMod">';
  		$html .= '	<div class="formMod">';
  		$html .= '		<div class="head-modulo row '.$class_modo.'">';
  		$html .= '			<div class="head-botones pull-left">';
  		$html .=   				$btnLeft;
  		$html .= '			</div>';
  		$html .= '			<h1 class="title">'.$title.'</h1>';
  		if ($botones_right!=""){
  			$html .= '		<div class="head-botones pull-right">';
  			$html .= $btnLight;
  			$html .= '		</div>';		
  		}
  		$html .= '		</div>';
  		
  		$html .= '	</div>';

  		switch ($modo) {
  			case 'mod':
  				$html .= '<div class="body-modulo container-fluid '.$class.'">';
  				break;
  			
  			default:
  				break;
  		}

  		return $html;
	}

	public function modFooter($vars){
		$modo = $vars["modo"];
		$html  = '';
		switch ($modo) {
			case 'mod':
				$html .= '	</div>';
				break;
			
			default:
				break;
		}
		$html .= '	</div>';
  		return $html;
	}

	public function tbody($tipo="",$id,$class=""){
		$html = '';

		switch ($tipo) {
			case 'container':
				$html .='<!-- Inicio tbody-container modulo -->';
				$html .='<div class="tbody tbody-container '.$class.'" id="'.$id.'">';
				$html .='	<div class="container">';
			break;

			case 'fin-container':
				$html .='	</div><!-- fin tbody container -->';
				$html .='</div><!-- fin tbody modulo -->';
				break;			

			case 'fin':
				$html .='</div>';
				$html .='<!-- fin tbody modulo -->';
				break;
			
			default:
				$html .='<!-- Inicio tbody modulo -->';
				$html .='<div class="tbody '.$class.'" id="'.$id.'">';
				break;
		}

		return $html;
	}	

	public function form($vars){
		$html = '';
		$tipo = $vars["tipo"];
		$class = $vars["class"];
		$id = $vars["id"];
		$method = $this->fmt->emptyReturn($vars["method"],"POST");

		switch ($tipo) {

			case 'fin':
				$html .='</form>';
				$html .='<!-- fin form modulo -->';
				break;
			
			default:
				$html .='<!-- Inicio form modulo -->';
				$html .='<form class="form form-modulo '.$class.'"  method="'.$method.'" id="'.$id.'">';
				break;
		}

		return $html;
	}

	public function head($vars){

		$modo = $this->fmt->emptyReturn($vars["modo"],"estandar");
		$title = $this->fmt->emptyReturn($vars["title"],"Titulo");
		$id = $vars["id"];
		$idMod = $vars["idMod"];
		$botonesRight = $vars["botonesRight"];
		$html='';

		if (!empty($idMod)){
			$modArray = $this->fmt->modulo->datosId($idMod);
			$title = $modArray["mod_nombre"]; 
			$rutaAmigable = $modArray["mod_ruta_amigable"]; 
			$color = $modArray["mod_color"]; 
			$icon = $modArray["mod_icono"]; 
			$html .= $this->titlePage(array('title' => $title));
		}
		
		switch ($modo) {
			case 'estandar':
				$html .='<div class="header headerEstandar" id="'.$id.'">'; 
				$html .='   	<div class="pull-left">'; 
				$html .='   		<i class="'.$icon.'"  style="color:'.$color.'" ></i>'; 
				$html .='			<label class="title"><h1>'.$title.'</h1></label>'; 
				$html .='		</div>'; 
				$html .='   	<div class="botonesRight pull-right">'.$botonesRight.'</div>'; 
				$html .='</div>'; 
				break;	

			case 'libre':
				$html .='<div class="header" id="'.$id.'">'; 
				$html .='   <div class="botonesLeft">'.$vars["botonesLeft"].'</div>'; 
				$html .='	<label class="title"><h1>'.$title.'</h1></label>'; 
				$html .='   <div class="botonesRight">'.$vars["botonesRight"].'</div>'; 
				$html .='</div>'; 
				break;	

			case 'tarea':
				$html .='<div class="header headerModoTarea" id="'.$id.'">'; 
				$html .='   <a class="btn btn-icon btn-accion-js" accion="'.$vars["accionBtnAction"].'" vars="'.$vars["vars"].'"><i class="icn icn-chevron-left"></i></a>'; 
				$html .='	<label class="title"><h1>'.$title.'</h1></label>'; 
				$html .='   <div class="botonesRight">'.$vars["botonesRight"].'</div>'; 
				$html .='</div>'; 
				break;
		}

		return $html;
	}

	//Modulo ADM

	public function headerBodyPage($vars){
        $class = $vars["class"];
        $aux .= '';
        $aux .= '<div class="body-page bodyPage animated fadeIn '.$class.'">';

        return $aux;
    }	

    public function footerBodyPage(){
        $aux .="";
        $aux .= '</div> <!-- body-page --> '."\n";

        return $aux;
    }

    //botones

    public function btn($vars){
		$id = $vars["id"];
		$item = $vars["idItem"];
		$idMod = $this->fmt->emptyReturn($vars["idMod"],0);
		$attr = $vars["attr"];
		$class = $vars["class"];
		$title = $vars["title"];
		$lang = $this->fmt->emptyReturn($vars["lang"],"es");
		$nom = $this->fmt->emptyReturn($vars["nombre"],"");
		$vari = $vars["vars"];
		$icon = $vars["icono"];
		$accion = $vars["accion"];

		return '<a class="'.$class.'" title="'.$title.'" id="'.$id.'" '.$attr.' idmod="'.$idMod.'" item="'.$item.'"  accion="'.$accion.'" nombre="'.$nom.'"  vars="'.$vari.'"><i class="'.$icon.'"></i><span lang="'.$lang.'">'.$nom.'</span></a>';
	}

	public function btnFormNuevo($vars){
		$idMod = $vars["idMod"];
		$nom = $vars["nombre"];
		$idRender= $vars["idRender"];
		$vars = 'fn:form_nuevo,idRender:'.$idRender.',idMod:'.$idMod; 
		$html  = '';
		$html .= $this->btn( array( 'idMod' => $idMod ,
									'icono' => 'icn icn-plus',
									'nombre' => $nom,
									'class' => 'btn btn-primary btn-accion-js',
									'accion'=> 'renderFuncionClase',
									'vars'=> $vars ));


		return $html;
	}


}