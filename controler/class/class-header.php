<?php
header('Content-Type: text/html; charset=utf-8');

class CLASSHEADER
{

    var $fmt;

    function __construct($fmt)
    {
        $this->fmt = $fmt;
    }

    function header_html($cat)
    {
        $aux  = '<!DOCTYPE html>' . "\n";
        $aux .= '<!-- Si te gusta programar este sitio esta desarrollado en zundi cms open sourse -> http://github.com/zundi -->' . "\n";
        $aux .= '<html id="page-index" lang="ES">' . "\n";
        $aux .= '<head>' . "\n";

        $aux .= '	<title>' . $this->nombreSitio($cat) . '</title>' . "\n";

        $aux .= '	<link rel="shortcut icon" href="' . _RUTA_WEB . $this->getFavicon($cat) . '" />' . "\n";  //Trabajar

        $aux .= '	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">' . "\n";
        $aux .= '	<meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">' . "\n";
        $aux .= '	<meta http-equiv="X-UA-Compatible" content="IE=10" />' . "\n";
        $aux .= '	<meta http-equiv="X-UA-Compatible" content="IE=9" />' . "\n";
        $aux .= '	<meta http-equiv="X-UA-Compatible" content="IE=8" />' . "\n";
        $aux .= '	<meta http-equiv="X-UA-Compatible" content="IE=7" />' . "\n\n";

        $aux .= '    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->' . "\n";
        $aux .= '    <!--[if lt IE 9]>' . "\n";
        $aux .= '    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>' . "\n";
        $aux .= '    <script src="js/respond.min.js"></script>' . "\n";
        $aux .= '    <![endif]-->' . "\n\n";

        //$aux .= $this->css()." \n";
        //$aux .= $this->js()." \n";

        return $aux;
    }

    function nombreSitio($cat)
    {
        if (_MULTIPLE_SITE == "off") {
            $nombre = $this->nombreConfig();
        } else {
            $nombre = $this->fmt->categoria->descripcionCat($cat);
        }

        if ($cat == 0) {
            $nombre = $this->nombreConfig();
        }
        return $nombre;
    }

    function getFavicon($cat)
    {
        if (_MULTIPLE_SITE == "off") {
            $favicon = $this->faviconConfig();
        } else {
            if (!empty($cat)) {
                $favicon = $this->fmt->categoria->faviconCategoria($cat);
            } else {
                $favicon = $this->faviconConfig();
            }
        }

        if ($cat == "0") {
            $favicon = $this->faviconConfig();
        }
        return $favicon;
    }

    function nombreConfig()
    {
        $consulta = "SELECT conf_nombre_sitio FROM configuracion";
        $rs = $this->fmt->query->consulta($consulta, __METHOD__);
        $fila = $this->fmt->query->obt_fila($rs);
        $nombre = $fila["conf_nombre_sitio"];
        if (empty($nombre)) {
            $nombre = _VZ;
        }
        return $nombre;
    }


    function faviconConfig()
    {
        $consulta = "SELECT conf_favicon FROM configuracion";
        $rs = $this->fmt->query->consulta($consulta, __METHOD__);
        $fila = $this->fmt->query->obt_fila($rs);
        $nombre = $fila["conf_favicon"];
        if (empty($nombre)) {
            $nombre = "src/img/favicon.png";
        }
        return $nombre;
    }

    function ruta_analitica()
    {
        //echo "ruta analitica";
        $consulta = "SELECT conf_meta FROM configuracion";
        $rs = $this->fmt->query->consulta($consulta, __METHOD__);
        $fila = $this->fmt->query->obt_fila($rs);
        $ruta = $fila["conf_meta"];
        // if (!empty($ruta)){
        //   return "    <meta ".$ruta." ej:analitica />"."\n";
        // }
        return $ruta;
    }

    function conf_script_head()
    {
        //echo "ruta analitica";
        $consulta = "SELECT conf_script_head FROM configuracion";
        $rs = $this->fmt->query->consulta($consulta, __METHOD__);
        $fila = $this->fmt->query->obt_fila($rs);

        return $fila["conf_script_head"];
    }

    function conf_script_footer()
    {
        //echo "ruta analitica";
        $consulta = "SELECT conf_script_footer FROM configuracion";
        $rs = $this->fmt->query->consulta($consulta, __METHOD__);
        $fila = $this->fmt->query->obt_fila($rs);
        $ruta = $fila["conf_script_footer"];
        // if (!empty($ruta)){
        //   return "    <meta ".$ruta." ej:analitica />"."\n";
        // }
        return $ruta;
    }

    function header_modulo($mod)
    {

        $aux .= "";
        $aux .= "<div class='body-page animated fadeIn'>";

        return $aux;
    }

    function padre_cat($cat)
    {
        if (isset($_GET["cat"])) {
            if (!is_numeric($_GET["cat"])) {
                $id_cat_padre = $this->traer_padre($_GET["cat"]);
            }
        } else {
            $id_cat_padre = 1;
        }
    }

    public function modulo($vars)
    {
        $class = $vars["class"];
        $aux .= "";
        $aux .= "<div class='body-page animated fadeIn " . $class . "'>";

        return $aux;
    }

    public function titlePage($pageTitle, $cat = 0)
    {
        if (!empty($this->nombreSitio($cat))) {
            $pageTitle = $pageTitle . " - " . $this->nombreSitio($cat);
        } else {
            $pageTitle = $pageTitle;
        }
        return '<script type="text/javascript">
            $(document).ready(function() {
              $("title").html("' . $pageTitle . '");
            });
          </script>';
    }

    function title_page($pageTitle, $cat = 0)
    {
        // $pageContents = ob_get_contents (); // Get all the page's HTML into a string
        // ob_end_clean (); // Wipe the buffer
        // // Replace <!--TITLE--> with $pageTitle variable contents, and print the HTML
        // //$pageTitle." - ".$this->nombre_sitio($cat)
        $pageTitle = $pageTitle . " - " . $this->nombreConfig($cat);
        return '<script type="text/javascript">
            $(document).ready(function() {
              $("title").html("' . $pageTitle . '");
            });
          </script>';
    }
}