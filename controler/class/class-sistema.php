<?php
header("Content-Type: text/html;charset=utf-8");

class SISTEMA{

	var $fmt;
	var $id_mod;
	var $id_item;
	var $id_estado;
    var $ruta_modulo;

	function SISTEMA($fmt,$id_mod=0,$id_item=0,$id_estado=0){
		$this->fmt = $fmt;
		$this->id_mod = $id_mod;
		$this->id_item = $id_item;
		$this->id_estado = $id_estado;
    	$this->ruta_modulo= _RUTA_WEB."dashboard/".$this->fmt->class_modulo->ruta_amigable_modulo($id_mod);
	}

	public function datosId($id){
	    $sql="SELECT * FROM sistema WHERE sis_id='".$id."' AND sis_activar='1'";
	    $rs =$this->fmt->query->consulta($sql);
	    $num=$this->fmt->query->numRegistros($rs);
	    if($num>0){ 
	      $row=$this->fmt->query->obtFila($rs);    
	      return $row;
	    }else{
	      return 0;
	    }
	    $this->fmt->query->liberarConsulta();
  	}

	public function sistemasModulosRol($idRol){
		$sistemasArray = $this->sistemaRol($idRol);
		$contSys = count($sistemasArray);
		for ($i=0; $i < $contSys ; $i++) { 
		   $sistema[$i]["sis_id"] = $sistemasArray[$i]["sis_id"];
		   $sistema[$i]["sis_nombre"] = $sistemasArray[$i]["sis_nombre"];
		   $sistema[$i]["modulos"] = $this->modulosSistema($sistemasArray[$i]["sis_id"]);
		}
		return $sistema;
	}

	public function sistemaRol($idRol){
    	$sql="SELECT sis_id,sis_nombre FROM sistema,sistema_roles WHERE sis_rol_rol_id='".$idRol."' AND sis_rol_sis_id=sis_id AND sis_activar='1' ORDER BY sis_orden ASC";
    	$rs = $this->fmt->query->consulta($sql,__METHOD__);
    	$num =$this->fmt->query->numRegistros($rs);
	    if($num>0){ 
	      for ( $i=0; $i < $num; $i++){
	        $row=$this->fmt->query->obtFila($rs); 
	        $sistema[$i]["sis_id"] = $row["sis_id"]; 
	        $sistema[$i]["sis_nombre"] = $row["sis_nombre"]; 
	      }
	      return  $sistema;
	    }else{
	      return 0;
	    }
	    $this->fmt->query->liberarConsulta();
  	}

  	public function modulosSistema($idSis){
	    $sql="SELECT DISTINCT sis_mod_mod_id,mod_id, mod_nombre,mod_icono,mod_color FROM sistema_modulos,sistema,modulo WHERE sis_mod_sis_id='".$idSis."' and mod_activar=1 and sis_mod_mod_id=mod_id ORDER BY sis_mod_orden ASC";
	    $rs = $this->fmt->query->consulta($sql,__METHOD__);
	    $num = $this->fmt->query->numRegistros($rs);
	    if ($num > 0){
	      for ( $i=0; $i < $num; $i++){
	        $row = $this->fmt->query->obtFila($rs);
	        $dat[$i]["mod_id"] = $row["mod_id"];
	        $dat[$i]["mod_nombre"] = $row["mod_nombre"];
	        $dat[$i]["mod_icono"] = $row["mod_icono"];
	        $dat[$i]["mod_color"] = $row["mod_color"];
	      }
	      return $dat;
	    }else{
	      return 0;
	    }
	    $this->fmt->query->liberarConsulta();
  	}
}