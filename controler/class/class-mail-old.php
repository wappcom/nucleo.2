<?PHP
header('Content-Type: text/html; charset=utf-8');


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MAIL
{

	var $fmt;

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	function __construct($fmt)
	{
		$this->fmt = $fmt;
	}

	function traer_smtp()
	{
		$row["smtp"] = _SMTP;
		$row["port"] = _PUERTO;
		$row["salida_correo"] = _CORREO;
		$row["contracena_sa"] = _PASSWORD_MAIL;
		$row["entrada_correo"] = _CORREO;
		$row["seguridad_mail"] = _SEGURIDAD_MAIL;
		return $row;
	}

	function enviar($correo, $nombre_c, $mensaje, $asunto, $nombre, $seguridad = "tls")
	{
		require_once (_RUTA_NUCLEO . "controler/includes/phpmailer/PHPMailerAutoload.php");

		$rw = $this->traer_smtp();

		$mail = new PHPMailer;

		$mail->SetLanguage('es');
		$mail->isSMTP();                                     // Set mailer to use SMTP
		//$mail->SMTPDebug = 0;
		$mail->Host = $rw['smtp'];  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $rw['salida_correo'];                 // SMTP username
		$mail->Password = $rw['contracena_sa'];                           // SMTP password
		$mail->SMTPSecure = $rw['seguridad_mail'];                               // Enable TLS encryption, `ssl` also accepted
		$mail->Port = $rw['port'];                                    // TCP port to connect to

		$mail->setFrom($rw['entrada_correo'], utf8_decode($nombre));

		//$mail->addReplyTo($rw['entrada_correo'], $nombre);
		$mail->addAddress($correo, $nombre_c);     // Add a recipient

		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = utf8_decode($asunto);
		$mail->Body = utf8_decode($mensaje);
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		$exito = $mail->Send();

		if (!$exito) {
			return false;
		} else {
			return true;
		}
	}



	public function send($vars)
	{
		$email = $vars["email"];
		$name = $vars["name"];
		$subject = $vars["subject"];
		$setFromMail = $vars["setFromMail"];
		$setFromName = $vars["setFromName"];
		$body = $vars["body"];
		$eUser = $vars["confirmReadingTo"];




		//Load Composer's autoloader
		require_once (_RUTA_NUCLEO . "vendor/PHPMailer/src/Exception.php");
		require_once (_RUTA_NUCLEO . "vendor/PHPMailer/src/PHPMailer.php");
		require_once (_RUTA_NUCLEO . "vendor/PHPMailer/src/SMTP.php");

		//Instantiation and passing `true` enables exceptions
		$mail = new PHPMailer(true);

		try {
			//Server settings
			$mail->SMTPDebug = 0;                      //Enable verbose debug output
			$mail->isSMTP();                                            //Send using SMTP
			$mail->Host = _SMTP;                     //Set the SMTP server to send through
			$mail->SMTPAuth = true;                                   //Enable SMTP authentication
			$mail->Username = _CORREO;                     //SMTP username
			$mail->Password = _PASSWORD_MAIL;                               //SMTP password
			//$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->SMTPSecure = _SEGURIDAD_MAIL;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port = _PUERTO;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			//Recipients
			$mail->setFrom($setFromMail, $setFromName);
			$mail->addAddress($email, $name);     //Add a recipient
			//$mail->addAddress('hterrazas@gmail.com', $name);     //Add a recipient
			//$mail->addReplyTo('hermany.terrazas@gmail.com', 'Information');
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');

			//Attachments
			//$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
			//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

			//Content
			$mail->IsHTML(true);                                //Set email format to HTML
			$mail->Subject = utf8_decode($subject);
			$mail->Body = utf8_decode($body);

			$mail->AddCustomHeader("X-Confirm-Reading-To: $eUser");
			$mail->AddCustomHeader("Return-Receipt-To: $eUser");
			$mail->AddCustomHeader("Disposition-Notification-To: $eUser");
			$mail->ConfirmReadingTo = $eUser;
			//$mail->msgHTML($body);
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();

			// retornar mensaje de $mail 

			//return $mail->ErrorInfo;
			return "send";
		} catch (Exception $e) {
			return "error,{$mail->ErrorInfo}";
		}
	}
}