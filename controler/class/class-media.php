<?php
header('Content-Type: text/html; charset=utf-8');

class MEDIA{

	var $fmt;

	function __construct($fmt) {
		$this->fmt = $fmt;
	}

	public function urlArchivoId($id){
		$consulta= "SELECT DISTINCT mul_url_archivo FROM multimedia WHERE mul_id='".$id."'";
		$rs = $this->fmt->query->consulta($consulta,__METHOD__);
    	$num = $this->fmt->query->num_registros($rs);
		$fila= $this->fmt->query->obt_fila($rs);
		return $fila['mul_url_archivo'];
	}
}