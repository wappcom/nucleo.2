<?php
header('Content-Type: text/html; charset=utf-8');

class SITIO{

	var $fmt;

	function __construct($fmt){
		$this->fmt = $fmt;
	}

	public function nombreSitio($vars){
		$cat = $this->fmt->emptyReturn($vars["cat"],"");

		if (_MULTIPLE_SITE=="off"){
			$nombre= $this->datosConfig("conf_nombre_sitio");
		}

		if ($cat==0 || $cat==""){
			$nombre= $this->datosConfig("conf_nombre_sitio");	
		} 
		return $nombre;
	}

	public function datosConfig($dato=""){

		$consulta = "SELECT * FROM configuracion";
		$rs = $this->fmt->query->consulta($consulta,__METHOD__);
		$row = $this->fmt->query->obt_fila($rs);
		if ($dato==""){ 
			return $row; 
		}else{
			switch ($dato) {
				case 'conf_nombre_sitio':
				 	$nombre = $row["conf_nombre_sitio"];
				 	if ( empty($nombre)){ $nombre = _VZ; }
					return $nombre;
				break;
			}
		}


	}

	public function rutaAmigableId($idSitio){
		$sql = "SELECT sitio_ruta_amigable FROM sitio WHERE sitio_id='" . $idSitio . "'";
		$rs = $this->fmt->query->consulta($sql, __METHOD__);
		$fila = $this->fmt->query->obt_fila($rs);
		$rutaAmigable = $fila["sitio_ruta_amigable"];

		if ($ra) {
			return  $rutaAmigable;
		} else {
			return 0;
		}
		$this->fmt->query->liberarConsulta($rs);
	}

}