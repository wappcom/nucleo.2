function seleccionar(campo) {
    if (campo.length < 1) {
        campo.LabelUsuario.display = none;
    }
}

function deseleccionarBuscar(campo) {
    if (campo.length < 1) {
        campo.LabelUsuario.display = block;
    }
}

function toggleId(id) {
    $("#" + id).fadeToggle(800);
    void(0);
    //$( "#page-content-wrapper" ).toggleClass( "on" );
}

function toggleIdCerrar(id, tiempo) {
    setTimeout(function() {
        $("#" + id).fadeOut(800);
        void(0);
    }, tiempo);
}

function redireccionar_tiempo(ruta, tiempo, target = '_self') {
    setTimeout(function() {
        //$("#" + id ).fadeOut(800);void(0);
        if (target == '_self') {
            document.location.href = ruta;
        } else {
            window.open(ruta, '_blank');
        }

    }, tiempo);
}

function return_view(url) {
    var result = null;
    var scriptUrl = url;
    $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
            result = data;
        }
    });
    return result;
}


$(document).ready(function() {

    $('.preloader-modulo').fadeOut('slow');

    $("#bs-menu .dropdown-toggle").click(function(e) {
        //alert ("aqui");
        var i = $(this).parent().index();
        $(this).parent().attr('id', 'dropdown-' + i);
        $activo = $("#dropdown-" + i).attr('toogle');
        //alert ($activo);
        if ($activo == "true") {
            $(".dropdown-menu").removeClass("on");
            $(".dropdown-toggle").removeClass("on");
            $(".dropdown").attr('toogle', 'false');
        } else {
            $(".dropdown[toogle=true] .dropdown-toggle").removeClass("on");
            $(".dropdown[toogle=true] .dropdown-menu").removeClass("on");
            $(".dropdown[toogle=true]").attr('toogle', 'false');
            $("#dropdown-" + i).attr('toogle', 'true');
            $("#dropdown-" + i + " .dropdown-toggle").addClass("on");
            $("#dropdown-" + i + " .dropdown-menu").addClass("on");
            $(".box-nav-modulos").removeClass("on");
        }
        resize();
        e.stopPropagation();
    });



    $('#content-page').on('click touchend', function(e) {
        //alert ("aqui");
        $(".dropdown-menu").removeClass("on");
        $(".dropdown-toggle").removeClass("on");
        $(".dropdown[toogle=true]").attr('toogle', 'false');
        $(".dropdown").attr('toogle', 'false');
        $(".box-nav-modulos").removeClass("on");
        $(".box-sites").removeClass("on");
        $(".btn-salto").removeClass("on");
        // $(".bloque-etiquetas").removeClass('on');
        // $(".box-cliente").removeClass('nivel');
    });



    function resize() {
        if (w < 470) {
            $(".collapse").addClass("on");
            $(".collapse-in").addClass("on");
        }
        if (w > 470) {
            $(".collapse").removeClass("on");
            $(".collapse-in").removeClass("on");
        }

        var hmr = $(".nav-menu .dropdown-menu").innerHeight();
        $(".box-nav-modulos").css("height", hm - 2 + "px");

        //console.log( w + " : " + h + " : hm "+ hm + " : Hmr " + hmr);

        if (h > hm) {
            $(".nav-menu .dropdown-menu").css("height", hm + "px");
            $(".nav-menu .dropdown-menu").css("overflow-y", "hidden");
        } else {
            $(".nav-menu .dropdown-menu").css("height", h - 32 + "px");
            $(".nav-menu .dropdown-menu").css("overflow-y", "auto");
        }
    }

    var idr;
    w = $(window).width();
    h = $(window).height();
    hp = 32; //padding caja
    hm = $(".nav-menu .dropdown-menu").innerHeight() + hp; // menu + espacio de barra

    //console.log(w + " : " + h + " : hm " + hm  );
    resize();

    $(window).resize(function() {
        clearTimeout(idr);
        idr = setTimeout(doneResizing, 100);
    });

    $('.disabled').attr("disabled", true);

    function doneResizing() {
        w = $(window).width();
        h = $(window).height();
        resize();
    }

});


function eliminarDiacriticos(texto) {
    return texto.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

var reemplazarAcentos = function(cadena) {
    var chars = {
        á: "a",
        é: "e",
        í: "i",
        ó: "o",
        ú: "u",
        à: "a",
        è: "e",
        ì: "i",
        ò: "o",
        ù: "u",
        ñ: "n",
        Á: "A",
        É: "E",
        Í: "I",
        Ó: "O",
        Ú: "U",
        À: "A",
        È: "E",
        Ì: "I",
        Ò: "O",
        Ù: "U",
        Ñ: "N",
    };
    var expr = /[áàéèíìóòúùñ]/gi;
    var res = cadena.replace(expr, function(e) {
        return chars[e];
    });
    return res;
};

function convertirUrlAmigable(text) {
    console.log("url");
    var text = eliminarDiacriticos(text);
    text = text.toLowerCase(); // a minusculas
    text = text.replace(/ /g, "-");

    text = text.replace("!", "");
    text = text.replace("'", "");
    text = text.replace("(", "");
    text = text.replace(")", "");
    text = text.replace("*", "");
    text = text.replace('"', "");
    // text = text.replace(';', '-');
    // text = text.replace('; ', '-');
    // text = text.replace(':', '-');
    // text = text.replace(': ', '-');
    text = text.replace('"', "");
    text = text.replace("'", "");
    text = text.replace(",", "");
    text = text.replace(/ [{()}:,;* ] /g, "");

    // text = text.replace(/[ýÿ]/, 'y');
    // text = text.replace(/[ñ]/, 'n');
    // text = text.replace(/[ç]/, 'c');
    // text = text.replace(/[&]/, 'y');
    // text = text.replace(/[ó]/, 'o')

    // text = text.replace(/[áàäâå]/, 'a');
    // text = text.replace(/[éèëê]/, 'e');
    // text = text.replace(/[íìïî]/, 'i');
    // text = text.replace(/[óòöô]/, 'o');
    // text = text.replace(/[úùüû]/, 'u');

    // // text = text.replace(/[^a-zA-Z0-9]/, '');

    // text = text.replace(/[,]/, '-');
    // text = text.replace(/['"`''""]/, '');
    text = text.replace(/['"`''"":;]/, "");

    // // text = text.replace(/(')s+/, 's');
    // // text = text.replace(/...+/, '-');
    // // text = text.replace(/(_)$/, '-');
    // // text = text.replace(/[?¡¿!(),:]/, '');
    // // text = text.replace(/(')$/, '');
    text = text.replace(/[.]$/, "");
    // text = text.replace(/[..]$/, '');
    // text = text.replace(/[...]$/, '');
    // text = text.replace(/[....]$/, '');
    // // text = text.replace(/(')$/, '');
    // text = text.replace(/(')$/, '');
    // text = text.replace(/(")$/, '');
    // text = text.replace(/[']$/, '');
    // text = text.replace(/["]$/, '');
    // text = text.replace(/(,)$/, '-');
    // text = text.replace(/(_)/, '');
    // text = text.replace(/^(:)/, '-');
    // text = text.replace(/ +/g, '-');
    // text = text.replace(/-+/g, '-');
    // text = text.replace(/&.,+?;/g, '-');

    texto = text.split("");

    //console.log(texto[ texto.length -1 ]);

    if (texto[texto.length - 1] == "-" || texto[texto.length - 1] == " ") {
        text = text.substring(0, texto.length - 1);
    }

    return text;
}