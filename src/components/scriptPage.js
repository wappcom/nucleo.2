$(document).ready(function() {


    $('body').on('click', '.btnActualizarMultimedia', function(event) {
        event.preventDefault();
        //console.log("btnActualizarMultimedia: "+sitioNucleo);
        var url = 'controler/ajax/ajax-mul-actualizar.php';
        const data = new FormData();
        data.append('url', url);
        data.append('inputId', $('#form-editar-mul #inputId').val());
        data.append('inputNombre', $('#form-editar-mul #inputNombre').val());
        data.append('inputLeyenda', $('#form-editar-mul #inputLeyenda').val());
        data.append('inputTextoalternativo', $('#form-editar-mul #inputTextoalternativo').val());
        data.append('inputDescripcion', $('#form-editar-mul #inputDescripcion').val());
        data.append('inputEmbed', $('#form-editar-mul #inputEmbed').val());
        data.append('inputDestino', $('#form-editar-mul #inputLink').val());
        data.append('inputDestino', $('#form-editar-mul #inputDestino').val());

        const ruta = sitio + 'ajax.php';
        var parametros = { method: 'POST', mode: 'cors', cache: 'default', body: data };

        fetch(ruta, parametros)
            .then(function(response) {
                if (response.ok) { return response.text(); } else { throw "Error en la llamada Ajax"; }
            })
            .then(function(msg) {
                console.log(msg);
                var msg = $.trim(msg);
                var dt = msg.split(",");
                $(".modal-editar-mul").removeClass("on");
            })
            .catch(function(err) {
                console.log(err);
            });
    });

    $(".btn-form").click(function() {
        var formk = $(this).attr("form");

        var act = '&estado-mod=' + $(this).val();
        var datos = $("#" + formk).serialize() + act;
        //console.log(datos);
        //alert(datos);
        $("#popup-div").html("");
        $(".footer-pag").removeClass("on");
        abrirModulo(datos);
    });

    $("body").on('click', '.btn-menu-ajax', function(e) {
        $(".modal-form").addClass("on");
        $(".body-page").css("overflow-y", "hidden");
        var variables = $(this).attr('vars');
        var id_mod = $(this).attr('id_mod');
        var ruta = 'ajax-adm';
        var datos = { ajax: ruta, inputIdMod: id_mod, inputVars: variables };
        abrirModulo(datos);
    });

    $('body').on('click', 'a.btn-m-activar', function(e) {
        e.preventDefault();
        var variables = $(this).attr('vars');
        var id_mod = $(this).attr('id_mod');
        var ruta = 'ajax-activar';
        var datos = { ajax: ruta, inputIdMod: id_mod, inputVars: variables };
        //alert(datos);
        accion_modulo(datos);
    });

    $("body").on('click', 'a.btn-m-eliminar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var nom = $(this).attr('nombre');
        var variablesx = $(this).attr('vars');
        var id_mod = $(this).attr('id_mod');

        $(".modal-form").addClass("on");
        $(".content-page").css("overflow-y", "hidden");
        $(".modal-form .modal-inner").addClass("mensaje-eliminar");
        $(".modal-form .modal-inner").html('<div class="modal-title"></div><div class="modal-body"> <i class="icn icn-trash"></i> <label>"' + nom + '" se eliminará, estas seguro de eliminarlo. </label><span>No podrás deshacer esta acción.<span> </div><div class="modal-footer"><a class="btn btn-cancelar btn-small btn-full">Cancelar</a><a class="btn btn-info btn-modal-m-eliminar btn-small" id_mod="' + id_mod + '" vars="' + variablesx + '" >Eliminar</a></div>');

        $(".btn-cancelar").on("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(".modal-form").removeClass("on");
            $(".modal-form .modal-inner").removeClass("mensaje-eliminar");
            $(".modal-form .modal-inner").html(" ");
            $(".content-page").css("overflow-y", "auto");
        });

        $(".btn-modal-m-eliminar").on("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            var variables = $(this).attr('vars');
            var id_mod = $(this).attr('id_mod');
            //var ruta='ajax-eliminar';
            var ruta = 'controler/ajax/ajax-eliminar.php';
            var datos = { url: ruta, inputIdMod: id_mod, inputVars: variables };
            //alert(variables);
            accion_modulo(datos);
        });
    });

    //nuevo sistema
    $("body").on('click', 'a.btn-accion', function(e) {
        e.preventDefault();
        e.stopPropagation();
        //variables
        var variables = $(this).attr('vars');
        //var ruta = 'controler/ajax/ajax-accion.php';
        //var datos = {  inputVars: variables };
        //var datos = {  inputVars: variables };


        //acciones
        //$(".modal-form").addClass("on");
        //$(".body-page").css("overflow-y", "hidden");
        //console.log(variables);
        //console.log(variables+" "+datos);
        accionModulo(variables);
    });

    $("body").on('click', 'a.btn-accion-js', function(e) {
        var accion = $(this).attr('accion');
        var vars = $(this).attr('vars');
        console.log("btn-accion-js / accion:" + accion + ",vars:" + vars);
        eval(accion + '("' + vars + '")');
    });

    //.btn-up-finder
    $("body").on('click', 'a.btn-up-finder', function(e) {
        var vars = $(this).attr("vars");
        lanzarFinder(vars);
    });

    // btns Eliminar
    $('body').on('click', '.btnEliminarItemTabla', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var item = $(this).attr("item");
        var idMod = $(this).attr("idMod");
        var title = $(this).attr("title");
        var accion = $(this).attr("accion");
        lanzarVentanaEliminar({ idItem: item, idMod: idMod, title: title, accion: accion });
    });

    // Acciones
    function accion_modulo(datos) {
        $.ajax({
            url: sitio + "ajax.php",
            type: "post",
            async: true,
            data: datos,
            success: function(msg) {
                console.log(msg);
                var msg = $.trim(msg);
                var cadena = msg.split(':');
                var accion = cadena[0];
                var id_item = cadena[1];
                var estado = cadena[2];
                var id_mod = cadena[3];
                var errores = cadena[4];
                switch (accion) {
                    case 'activar':
                        //console.log(estado);
                        $("#btn-p-" + id_mod + "-" + id_item + " i").removeClass();
                        if (estado == 1) {
                            $("#btn-p-" + id_mod + "-" + id_item + " i").addClass("icn-eye-open");
                            $("#btn-p-" + id_mod + "-" + id_item).attr("vars", "activar," + id_item + ",0");
                        } else {
                            $("#btn-p-" + id_mod + "-" + id_item + " i").addClass("icn-eye-close");
                            $("#btn-p-" + id_mod + "-" + id_item).attr("vars", "activar," + id_item + ",1");
                        }
                        $(".content-page").css("overflow-y", "auto");
                        break;
                    case 'eliminar':
                        //console.log(id_item);
                        $(".modal-form").removeClass("on");
                        $(".modal-form .modal-inner").removeClass("mensaje-eliminar");
                        $(".modal-form .modal-inner").html("");
                        $(".row-" + id_item).addClass("removiendo");
                        $("#btn-nav-sis-" + id_item).addClass("removiendo");
                        setTimeout(function() {
                            $(".row-" + id_item).remove();
                            $("#btn-nav-sis-" + id_item).remove();
                            $(".content-page").css("overflow-y", "auto");
                        }, 1500);

                        break;
                    default:
                        alert("no hay una acción determinada, revisar error base de datos " + errores);
                        break;
                }
            }
        });
    };

    function abrirModulo(datos) {
        //console.log("abrir_modulos:"+datos);
        //console.log(sitio);
        $.ajax({
            url: sitio + "ajax.php",
            type: "post",
            data: datos,
            cache: false,
            async: true,
            success: function(msg) {
                //console.log("ajx:"+msg);
                $("#modal .modal-inner").html(msg);
                // $('.preloader-page').fadeOut('slow');
                // console.log("hola");
            },
            complete: function() {
                $('.preloader-page').fadeOut('slow');

                var wbm = $(".modal .modal-inner").height();
                var wbmx = wbm - 108;
                // console.log("body-modulo:"+wbmx);
                $(".modal-inner .body-modulo").height(wbmx);
            },
            error: function() {
                alert("Ha ocurrido un error .abrirModulo");
            }

        });
    }

});



//lanzadores
lanzarRenderContenido = function(vars) {
    $(".content-page .root-modulo").html(renderContenido(vars));
}

//Eliminar tabla
lanzarVentanaEliminar = function(vars) {
    //console.log(vars);
    var idItem = vars.idItem;
    var title = vars.title;
    var idMod = vars.idMod;
    var accion = vars.accion;
    var vari = 'fn:eliminarItem,accion:eliminarItem,idMod:' + idMod + ',idItem:' + idItem;
    //console.log(vari);


    $(".content-page").prepend(renderModalEliminar({ accion: accion, title: title, variables: vari, id: 'modalEliminarItem' }));

    $(".btn-cancelar").on("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        cerrarModal();
    });
}

eliminarItemTabla = function(vars) {
    //var datos = vars;
    //console.log(datos);
    // const data = new FormData();
    // data.append('vars', datos);
    // data.append('file', "controler/fetch/eliminar-idtabla.php");

    // const ruta = sitio + 'controler/fetch/file-nucleo.php';
    // var parametros = { method: 'POST',mode:'cors', cache: 'default', body: data };

    // fetch( ruta , parametros)
    // .then(function(response) {
    //    if(response.ok) { return response.text(); } else { throw "Error en la llamada Ajax";}
    // })
    // .then(function(msg) {
    //    console.log("eliminarItemTabla :"+msg);
    //    var msg = $.trim(msg);
    //    var dt = msg.split(",");
    // })
    // .catch(function(err) {
    //    console.log(err);
    // });
    //return ;
    renderFuncionClase(vars);
}

//Acciones
accionModulo = function(datos) {
    //var vars = JSON.stringify(datos);
    //var vars = JSON.parse(datos);
    var vars = datos;
    //var urlParams  =  new URLSearchParams( datos.inputVars );
    //var tarea = urlParams.get('tarea');

    //console.log(datos);
    // console.log(sitio);
    // console.log(vars);
    const data = new FormData();
    data.append('vars', vars);

    // const myHeaders = new Headers();
    const ruta = sitio + 'controler/fetch/local.php';
    var parametros = { method: 'POST', mode: 'cors', cache: 'default', body: data };

    fetch(ruta, parametros)
        .then(function(response) {
            if (response.ok) { return response.text(); } else { throw "Error en la llamada fetch"; }
        })
        .then(function(msg) {
            console.log("AccionModulo: " + msg);
            var msg = $.trim(msg);
            var msg = JSON.parse(msg);
            var accion = msg.accion;
            var accionOut = msg.accionOut;
            var accionOutSec = msg.accionOutSec;
            var tarea = msg.tarea;
            var html = msg.html;
            var idMod = msg.idMod;
            var texto = '';


            if (accion == "eliminarItem") {
                var accionOutSec = accionOut.split(",");
                if (accionOutSec[0] == "realizado") {
                    if (accionOutSec[1]) {
                        var itemTemp = accionOutSec[1];
                        cerrarModal();
                        $(".row-" + itemTemp).addClass("removiendo");
                        setTimeout(function() {
                            $(".row-" + itemTemp).remove();
                            $(".content-page .root-modulo").css("overflow-y", "auto");
                        }, 1500);
                    }
                }
            }

            if (accionOut == "error") {
                //console.log(accionOutSec);
                cerrarModal();
                texto = accionOutSec;
                $(".content-page .root-modulo").prepend(renderModalAlert({ icono: 'icn icn-danger', label: texto, id: 'modalAlertErrores' }));

                $("#btn-modalAlertErrores").click(function() {
                    console.log("#btn-modalAlertErrores");
                    cerrarModal();
                });
            }

            if (accionOut == "lanzarVentanaEliminar") {
                var vars = { idItem: msg.idItem, title: msg.title, idMod: idMod };
                //console.log(vars);
                lanzarVentanaEliminar(vars);
            }

            if (accionOut == "renderContenido") {
                var vars = { html: html, idMod: idMod, id: tarea };
                lanzarRenderContenido(vars);
            }


        })
        .catch(function(err) {
            console.log(err);
        });
}

renderFuncionClase = function(vars) {
    //console.log("prueba");
    const data = new FormData();
    data.append('vars', vars);

    const ruta = sitio + 'controler/fetch/main.php';
    var parametros = { method: 'POST', mode: 'cors', cache: 'default', body: data };

    fetch(ruta, parametros)
        .then(function(response) {
            if (response.ok) { return response.text(); } else { throw "Error en la llamada fetch"; }
        })
        .then(function(msg) {
            console.log("renderFuncionClase: " + msg);
            var msg = $.trim(msg);
            var msg = JSON.parse(msg);

            if (msg.accion == "cargarAccion") {
                var vars = { html: msg.html, id: "content-" + msg.idRender };
                $(".content-page #" + msg.idRender).html(renderContenido(vars));
            }

            if (msg.accion == "eliminarItem") {
                cerrarModal();
                $(".row-" + msg.idItem).addClass("removiendo");
                setTimeout(function() {
                    $(".row-" + msg.idItem).remove();
                    $(".content-page .root-modulo").css("overflow-y", "auto");
                }, 1500);
            }
        })
        .catch(function(err) {
            console.log(err);
        });
}

//Modal
cerrarModal = function() {
    $(".modal-form").removeClass("on");
    $(".modal-form .modal-inner").removeClass("mensaje-eliminar");
    $(".modal-form .modal-inner").html(" ");
    $(".content-page").css("overflow-y", "auto");
    $(".content-page .root-modulo").css("overflow-y", "auto");

    $("#modalAlertErrores").remove();
    $("#modalEliminarItem").remove();
}

// finder
lanzarFinder = function(vars) {
    console.log("Lanzar finder...");
    var arrayVars = decodeCadenaToArray(vars);
    //console.log(arrayVars.idRender);
    var idRender = arrayVars.idRender;
    var renderFinderContenido = "";

    const data = new FormData();
    data.append('vars', JSON.stringify(arrayVars));

    if (arrayVars.upload == "imagen") {
        renderFinderContenido = renderFinderTabs({ upload: arrayVars.upload });
    }

    $("#" + idRender).append('<div class="modal modal-finder on"><div class="modal-finder-inner finder">' + renderFinderContenido + '</div></div>');

    const ruta = sitio + 'controler/fetch/finder.php';
    var parametros = { method: 'POST', mode: 'cors', cache: 'default', body: data };

    fetch(ruta, parametros)
        .then(function(response) {
            if (response.ok) { return response.text(); } else { throw "Error en la llamada fetch"; }
        })
        .then(function(msg) {
            console.log("lanzarFinder: " + msg);
            var msg = $.trim(msg);
        })
        .catch(function(err) {
            console.log(err);
        });
}

//decode
decodeCadenaToArray = function(vars) {
    var cadena = vars;
    var arrayC = cadena.split(",");
    var aux_array = "";
    var aux = "";
    var cadenaA = "";
    var cadenaA2 = "";
    var coun = arrayC.length;

    for (var i = 0; i < coun; i++) {
        var aux_cadena = arrayC[i];
        if (i == 0) {
            aux = '';
        } else {
            aux = '","';
        }
        aux_array += aux + aux_cadena.replace(":", "\":\"");
    }

    cadenaA = '{"' + aux_array + '"}';
    return JSON.parse(cadenaA);


    // console.log (vars);
    // console.log (cadenaA);
    // console.log (cadenaA2);
}