renderModalConfirm = function(vars){
	var label = vars.label;
	var vari = vars.vars;
	var icono = vars.icono;
	var accion = vars.accion;
	var id = vars.id;
	var classBtnAccion = vars.classBtnAccion;
	var labelBtnAccion = vars.labelBtnAccion;

	return `<div class="modalRender modalConfirm " id="`+id+`" >
				<div class="inner animated bounceIn fast">
					<div class="modal-title"></div>
					<div class="modal-body"> 
						<i class="`+icono+`"></i> 
						` + label + ` 
					</div>
					<div class="modal-footer">
						<a class="btn btn-cancelar btn-small btn-full">Cancelar</a>
						<a accion="`+accion+`" class="btn `+ classBtnAccion +` btn-small" vars="` + vari + `" >`+ labelBtnAccion +`</a>
					</div>
				</div>
			</div>`;
}

renderModalAlert = function(vars){
	var label = vars.label;
	var icono = vars.icono;
	var id = vars.id;
	var classBtnAccion = vars.classBtnAccion;

	return `<div class="modalRender modalAlert" id="`+id+`" >
				<div class="inner animated bounceIn fast">
					<div class="modal-title"></div>
					<div class="modal-body"> 
						<i class="`+icono+`"></i> 
						` + label + ` 
					</div>
					<div class="modal-footer">
						<a id="btn-`+id+`" class="btn btn-cancelar btn-info btn-small" >Aceptar</a>
					</div>
				</div>
			</div>`;
}

renderModalEliminar = function(vars){
	var label = `<label>"` + vars.title + `" se eliminará, estas seguro de eliminarlo. </label><span>No podrás deshacer esta acción.<span>`;
	return renderModalConfirm({ label:label,
								id : vars.id,
								vars: vars.variables,
								accion:vars.accion,
								labelBtnAccion:'Eliminar',
								classBtnAccion:' btn-danger btn-accion-js ',
								icono:'icn icn-trash icn-danger' });
}

renderContenido = function(vars){
	var html = vars.html;
	var id = vars.id;
	return `
		<div class="content-inner  animated fadeIn" id="`+id+`">`+html+`</div>
	`;
}

renderFinderTabs = function(vars){
	var upload = vars.upload;
	return `
		<div class="finder-head">   
			<div class="group-tabs">     
				<div class="tab">       
					<label class="title title-finder">FINDER</label>       
					<span class="group">         
						<a class="category active" id="tab-todos" idtab="todos" style="display: inline-block;">Todos</a>         
						<a class="category" id="tab-imagenes" idtab="imagenes" style="display: inline-block;">Imagenes</a>         
						<a class="category" id="tab-albums" idtab="albums" style="display: none;">Albums</a>         
						<a class="category" id="tab-videos" idtab="videos" style="display: none;">Videos</a>         
						<a class="category" id="tab-audio" idtab="audio" style="display: none;">Audio</a>         
						<a class="category" id="tab-documentos" idtab="documentos" style="display: none;">Documentos</a>       
					</span>     
				</div>   
			</div>
		</div>
	`;
}

renderModal = function(vars){
	var clase = vars.class;
	var item = vars.item;
	var contenido = vars.contenido;
	return `<div class="modal `+clase+`" item="`+item+`"><div class="inner">`+contenido+`</div></div>`;
}